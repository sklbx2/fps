// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraShakeBase.h"

#include "FPS_ProceduralAnimationComponent.h"
#include "FPS_InventoryComponent.h"
#include "FPS_WeaponComponent.h"
#include "FPS_CharacterHealthComponent.h"

#include "Engine/DamageEvents.h"

#include "Net/UnrealNetwork.h"


// Sets default values
AFPS_Character::AFPS_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating First-Person Structure
	Root_1P = CreateDefaultSubobject<USceneComponent>(TEXT("Root_1P"));
	Root_1P->SetupAttachment(RootComponent);

	RootShake_1P = CreateDefaultSubobject<USceneComponent>(TEXT("RootShake_1P"));
	RootShake_1P->SetupAttachment(Root_1P);

	MeshRoot_1P = CreateDefaultSubobject<USpringArmComponent>(TEXT("MeshRoot_1P"));
	MeshRoot_1P->SetupAttachment(RootShake_1P);
	MeshRoot_1P->TargetArmLength = 0.f;
	MeshRoot_1P->bDoCollisionTest = false;
	MeshRoot_1P->bUsePawnControlRotation = true;

	Mesh_1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh_1P"));
	Mesh_1P->SetupAttachment(MeshRoot_1P);
	Mesh_1P->SetRelativeLocation(FVector(-15.f, 0.f, -95.f));	// Defalut arms offset
	Mesh_1P->SetOnlyOwnerSee(true);
	Mesh_1P->bCastDynamicShadow = false;
	Mesh_1P->CastShadow = false;
	Mesh_1P->SetCollisionProfileName("NoCollision");

	GetMesh()->SetOwnerNoSee(true);

	CameraRoot_1P = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraRoot_1P"));
	CameraRoot_1P->SetupAttachment(RootShake_1P);
	CameraRoot_1P->TargetArmLength = 0.f;
	CameraRoot_1P->bDoCollisionTest = false;
	CameraRoot_1P->bUsePawnControlRotation = true;
	CameraRoot_1P->bInheritRoll = false;
	CameraRoot_1P->CameraLagMaxDistance = 10.f;

	CameraSkel_1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CameraSkel_1P"));
	CameraSkel_1P->SetupAttachment(CameraRoot_1P);
	CameraSkel_1P->bCastDynamicShadow = false;
	CameraSkel_1P->CastShadow = false;
	CameraSkel_1P->SetCollisionProfileName("NoCollision");

	Camera_1P = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera_1P"));
	Camera_1P->SetupAttachment(CameraSkel_1P, "Camera");
	Camera_1P->SetRelativeLocation(FVector(-10.f, 0.f, 60.f));	// Default camera offset
	Camera_1P->bUsePawnControlRotation = false;


	// Default values

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, StandHeight);

	// Pawn settings
	// Don't rotate character with camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.f;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 500.f, 0.f);
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	UpdateSpeed();

	// Components

	ProceduralAnimComponent = CreateDefaultSubobject<UFPS_ProceduralAnimationComponent>(TEXT("ProceduralAnimComponent"));

	WeaponComponent = CreateDefaultSubobject<UFPS_WeaponComponent>(TEXT("WeaponComponent"));
	if (WeaponComponent)
	{
		WeaponComponent->SetNetAddressable(); // Make DSO components net addressable
		WeaponComponent->SetIsReplicated(true); // Enable replication by default
		WeaponComponent->OnWeaponCreated.AddDynamic(this, &AFPS_Character::UpdateWeapon);
	}

	InventoryComponent = CreateDefaultSubobject<UFPS_InventoryComponent>(TEXT("InventoryComponent"));

	HealthComponent = CreateDefaultSubobject<UFPS_CharacterHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->SetNetAddressable(); // Make DSO components net addressable
		HealthComponent->SetIsReplicated(true); // Enable replication by default
		HealthComponent->OnDeath.AddDynamic(this, &AFPS_Character::Die);
		HealthComponent->OnHealthChange.AddDynamic(this, &AFPS_Character::UpdateSpeed);
	}


	// Network
	bReplicates = true;
}

//void AFPS_Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//
//	DOREPLIFETIME(AFPS_Character, InventoryComponent);
//	DOREPLIFETIME(AFPS_Character, WeaponComponent);
//}

// Called when the game starts or when spawned
void AFPS_Character::BeginPlay()
{
	Super::BeginPlay();	

	// Dear Code Reviewer.
	/* 
	  Kindly be informed that this approach serves a noble purpose in resolving the lamentable absence of a Controller
	during the occasion of BeginPlay (which may transpire upon respawn). 
	  An alternative approach could have been (as example) overloading Controller::OnPossessedBy(). However, as this method
	is invoked on the server, it would regrettably necessitate the intricate orchestration of rectifying the delay in 
	the Controller's possession on the clients.
	  The chosen path remains steadfast, impervious to the caprices of the varied order of class creation/Events on both 
	the clients and the server during game entry and while the game is afoot. And encapsulating all critical logic within
	the confines of the class itself.
	*/
	GetWorld()->GetTimerManager().SetTimer(WaitForController, this, &AFPS_Character::TrueBeginPlay, 1.f/16, true);
	
	TrueBeginPlay();
}

void AFPS_Character::TrueBeginPlay()
{
	if (GetController())
	{
		GetWorld()->GetTimerManager().ClearTimer(WaitForController);
	}
	else return;

	if (auto* P = GetProceduralAnimComponent())
	{
		// Setting up references in Procedural Animation Component
		P->Init(
			GetFirstPersonRoot(),
			GetFirstPersonRootShake(),
			GetFirstPersonCameraRoot(),
			GetFirstPersonMeshRoot(),
			GetFirstPersonMesh(),
			GetMesh(),
			GetCharacterMovement());

		// First Person
		if (IsLocallyControlled())
		{
			P->OnCrouchUpdate.AddDynamic(this, &AFPS_Character::SetCrouchAlpha_OnServer);
			P->OnLeanUpdate.AddDynamic(this, &AFPS_Character::SetLeanCoef_OnServer);
			P->OnAimOffsetUpdate.AddDynamic(this, &AFPS_Character::SetAimOffset_OnServer);
		}
		else // ThirdPerson
		{
			// This is excessively, due to 'bLocal' in ProceduralAnimComponent
			/*P->bCrouchAnim = false;
			P->bDipAnim = false;
			P->bWalkArmsAnim = false;
			P->bSwayAnim = false;
			P->bLeanAnim = false;
			P->bWalkCameraAnim = false;
			P->bBreathAnim = false;*/
		}
	}

	if (IsLocallyControlled())
	{
		GetWeaponComponent()->OnWeaponFired.AddDynamic(this, &AFPS_Character::TakeRecoilFromFire);
	}

	if (HasAuthority())
	{
		if (auto* W = GetWeaponComponent())
		{
			if (auto* I = GetInventoryComponent())
			{
				W->Init_OnServer(GetFirstPersonMesh(), GetMesh(), InventoryComponent);
				I->Init_OnServer(GetFirstPersonMesh(), GetMesh(), WeaponComponent);
			}
		}
	}	
}


// Called every frame
void AFPS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFPS_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Enhanced Input Controller
	if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EIC->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AFPS_Character::InputMove);
		EIC->BindAction(LookAction, ETriggerEvent::Triggered, this, &AFPS_Character::InputLook);
		LookActionValue = &EIC->BindActionValue(LookAction);

		EIC->BindAction(JumpAction, ETriggerEvent::Started, this, &AFPS_Character::InputJumpStarted);
		EIC->BindAction(JumpAction, ETriggerEvent::Completed, this, &AFPS_Character::InputJumpCompleted);

		EIC->BindAction(CrouchAction, ETriggerEvent::Started, this, &AFPS_Character::InputCrouch);
		EIC->BindAction(CrouchAction, ETriggerEvent::Completed, this, &AFPS_Character::InputCrouch);

		LeanActionValue = &EIC->BindActionValue(LeanAction);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("'%s' Failed to find an Enhanced Input Component!"), *GetNameSafe(this));
	}
}


float AFPS_Character::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventIstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventIstigator, DamageCauser);
	
	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		const FRadialDamageEvent* RadialDamage = static_cast<const FRadialDamageEvent*>(&DamageEvent);
		GetCharacterMovement()->AddImpulse((GetActorLocation()- RadialDamage->Origin).GetSafeNormal() * DamageAmount * 250.f);
	}
	else
	{
		GetCharacterMovement()->AddImpulse(DamageCauser->GetActorForwardVector() * DamageAmount * 1500.f);
	}
	
	if (GetHealthComponent()->IsAlive())
	{
		NotifyWasHit_Multicast(EventIstigator);
		SetLastDamageInstigator(EventIstigator);

		HealthComponent->ChangeCurrentHealth_OnServer(-ActualDamage);

		UpdateSpeed();
	}
	

	return ActualDamage;
}


//----	Movement	----//	//----	Movement	----//	//----	Movement	----//

void AFPS_Character::OnJumped_Implementation()
{
	Super::OnJumped_Implementation();

	// Calling Dip function with default values
	GetProceduralAnimComponent()->Dip();
}

void AFPS_Character::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);

	// Calling Dip function for landing
	GetProceduralAnimComponent()->Dip(true);
}


//---- Weapon ----//	//---- Weapon ----//	//---- Weapon ----//

void AFPS_Character::UpdateWeapon()
{
	if (IsLocallyControlled())
	{
		if (GetFirstPersonMesh()->GetAnimInstance())
		{
			if (GetFirstPersonMesh()->GetAnimInstance()->GetClass()->ImplementsInterface(USetABPArmsVarsInterface::StaticClass()))
			{
				ISetABPArmsVarsInterface::Execute_UpdateWeapon(GetFirstPersonMesh()->GetAnimInstance());
			}
		}
	}
	
}

void AFPS_Character::TakeRecoilFromFire(int32 CurrentAmmo)
{
	UGameplayStatics::PlayWorldCameraShake(GetWorld(), CameraShakeFire, GetCamera()->GetComponentLocation(), 0.f, 30.f);

	auto Recoil = GetWeaponRecoil();
	AddControllerYawInput((Recoil.KickSide + Recoil.RotateSide) * (FMath::RandBool() ? 0.01f : -0.01f) * GetCurrentAccuracy());
	AddControllerPitchInput(-0.02 * (Recoil.KickUp + Recoil.RotateUp) * GetCurrentAccuracy());
	

	if (GetFirstPersonMesh()->GetAnimInstance())
	{
		if (GetFirstPersonMesh()->GetAnimInstance()->GetClass()->ImplementsInterface(USetABPArmsVarsInterface::StaticClass()))
		{
			ISetABPArmsVarsInterface::Execute_SetCurrentAccuracy(GetFirstPersonMesh()->GetAnimInstance(), GetCurrentAccuracy());
		}
	}
}


//---- Damage ----//	//---- Damage ----//	//---- Damage ----//

void AFPS_Character::Die()
{
	// Function called by multicast broadcast from HealthComponent


	float AnimLength = 0.1f;
	int8 rand = FMath::RandHelper(DeathAnimations.Num());

	// All
	if (DeathAnimations.IsValidIndex(rand))
	{
		auto* Anim = DeathAnimations[rand];
		if (Anim && GetMesh() && GetMesh()->GetAnimInstance())
		{
			AnimLength = Anim->GetPlayLength() - 0.5f;
			GetMesh()->GetAnimInstance()->Montage_Play(Anim);
		}
	}
	

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Ignore);
		//GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}

	GetWeaponComponent()->GetWeapon()->SetFire(false);



	// First person
	if (IsLocallyControlled())
	{
		GetProceduralAnimComponent()->DestroyComponent();
		GetProceduralAnimComponent()->bCrouchAnim = false;
		GetProceduralAnimComponent()->bDipAnim = false;
		GetProceduralAnimComponent()->bWalkArmsAnim = false;
		GetProceduralAnimComponent()->bSwayAnim = false;
		GetProceduralAnimComponent()->bLeanAnim = false;
		GetProceduralAnimComponent()->bWalkCameraAnim = false;
		GetProceduralAnimComponent()->bBreathAnim = false;

		
		GetWeaponComponent()->GetWeapon()->GetFirstPersonMesh()->DestroyComponent();
		GetWeaponComponent()->GetWeapon()->GetFirstPersonMesh()->SetOwnerNoSee(true);
		GetWeaponComponent()->GetWeapon()->GetThirdPersonMesh()->SetOwnerNoSee(false);
		GetWeaponComponent()->DestroyComponent();

		GetFirstPersonMesh()->DestroyComponent();
		GetFirstPersonMesh()->SetOwnerNoSee(true);
		GetMesh()->SetOwnerNoSee(false);

		FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, true);
		Camera_1P->AttachToComponent(GetMesh(), Rule, FName("head"));
	}

	// Server
	if (HasAuthority())
	{
		RecieveOnDeath(GetLastDamageInstigator());

		GetWorldTimerManager().SetTimer(RagdollTimer, this, &AFPS_Character::EnableRagdoll_Multicast, AnimLength, false);
		SetLifeSpan(20.f);
		GetWeaponComponent()->GetWeapon()->SetLifeSpan(20.f);
	}

	GetProceduralAnimComponent()->DestroyComponent();
	//GetWeaponComponent()->DestroyComponent();
	//GetInventoryComponent()->DestroyComponent();
}

void AFPS_Character::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionProfileName("PhysicsActor");
		GetMesh()->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		//GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}



//----	Getters	----//	//----	Getters	----//	//----	Getters	----//

float AFPS_Character::GetCurrentAccuracy() const
{
	if (GetWeaponComponent() && GetWeaponComponent()->GetWeapon())
	{
		float CurrentAccuracy = GetWeaponComponent()->GetWeapon()->GetWeaponState().CurrentDispersion;
		float CrouchCoef = 1.f;
		if (GetProceduralAnimComponent())
		{
			CrouchCoef = FMath::GetMappedRangeValueClamped(TRange<float>(0.f, 1.f), TRange<float>(1.f, 0.5f), GetProceduralAnimComponent()->GetCrouchAlpha());
		}
		float MovementCoef = 1 + (UKismetMathLibrary::VSizeXY(GetVelocity()) / DefaultSpeed);
		return CurrentAccuracy * CrouchCoef * MovementCoef;
	}
	else return 1.f;
}


//----	Interface	----//	//----	Interface	----//	//----	Interface	----//

// ICombatInterface

void AFPS_Character::NotifyOnHit_Implementation()
{
	if (GetHealthComponent()->IsAlive())
	{
		if (auto* W = GetWeaponComponent()->GetWeapon())
		{
			W->NotifyOnHit();
		}
	}
	

	//RecieveOnHit();
	//OnHit.Broadcast();
}

// IFPS_ProceduralAnimInterface

void AFPS_Character::ChangeSpeedBy_Implementation(float MultiplyBy)
{
	if (GetHealthComponent()->IsAlive())
	{
		SpeedCoef *= MultiplyBy;
		UpdateSpeed();
	}
}

void AFPS_Character::UpdateCapsuleHeight_Implementation(float CrouchAlpha)
{
	if (GetHealthComponent()->IsAlive())
	{
		float NewHeight = FMath::Lerp(StandHeight, CrouchHeight, CrouchAlpha);
		GetCapsuleComponent()->SetCapsuleHalfHeight(NewHeight);
		SetCapsuleHeight_OnServer(NewHeight);
	}
}

FVector2D AFPS_Character::GetLookInput_Implementation() const
{
	if (LookActionValue && GetHealthComponent()->IsAlive())
	{
		return LookActionValue->GetValue().Get<FVector2D>();
	}
	else return FVector2D();
}

float AFPS_Character::GetLeanInput_Implementation() const
{
	if (LeanActionValue && GetHealthComponent()->IsAlive())
	{
		return LeanActionValue->GetValue().Get<float>();
	}
	else return 0.f;
}

FTransform AFPS_Character::GetThirdPersonLeftHand_Implementation() const
{
	if (GetHealthComponent()->IsAlive())
	{
		if (auto* W = GetWeaponComponent()->GetWeapon())
		{
			const FTransform& LH = W->GetLeftHandSocket(false);
			FVector Location;
			FRotator Rotation;

			GetMesh()->TransformToBoneSpace(FName("hand_r"), LH.GetLocation(), LH.Rotator(), Location, Rotation);

			return FTransform(Rotation, Location, LH.GetScale3D());
		}
	}

	// else
	return FTransform();
}


// IFPS_ProceduralAnimABPInterface

FTransform AFPS_Character::GetRightHand_Implementation() const
{
	if (GetHealthComponent()->IsAlive())
	{
		if (auto* W = GetWeaponComponent()->GetWeapon())
		{
			return W->GetRightHand();
		}
	}

	// else
	return FTransform();
}

FTransform AFPS_Character::GetRightHandCrouchOffset_Implementation() const
{
	if (GetHealthComponent()->IsAlive())
	{
		if (auto* W = GetWeaponComponent()->GetWeapon())
		{
			return W->GetRightHandCrouchOffset();
		}
	}

	// else
	return FTransform();
}

FTransform AFPS_Character::GetLeftHand_Implementation() const
{
	if (GetHealthComponent()->IsAlive())
	{
		if (auto* W = GetWeaponComponent()->GetWeapon())
		{
			const FTransform& LH = W->GetLeftHandSocket();
			FVector Location;
			FRotator Rotation;

			GetFirstPersonMesh()->TransformToBoneSpace(FName("hand_r"), LH.GetLocation(), LH.Rotator(), Location, Rotation);

			return FTransform(Rotation, Location, LH.GetScale3D());
		}
	}

	// else
	return FTransform();
}

FRotator AFPS_Character::GetAimRotation_Implementation() const
{
	int32 Distance = 5000;
	FVector BeginPont = GetCamera()->GetComponentLocation();
	FVector AimPoint = BeginPont + GetCamera()->GetForwardVector() * Distance;

	if (GetHealthComponent()->IsAlive())
	{
		// Because we will rotate character's hand, but weapon shoots from muzzle,
		// We must adjust aiming point for the displacement of the muzzle relative to the hand
		if (auto* W = GetWeaponComponent()->GetWeapon())
		{
			FVector Offset = W->GetActorLocation() - W->GetMuzzleSocket().GetLocation();
			AimPoint += Offset;

			BeginPont = W->GetActorLocation();
		}
	}

	return UKismetMathLibrary::FindLookAtRotation(BeginPont, AimPoint);
}

FProceduralAnimationValues AFPS_Character::GetProcAnimValues_Implementation() const
{
	if (GetHealthComponent()->IsAlive())
	{
		// Syntax sugar
		if (auto* P = GetProceduralAnimComponent())
		{
			return FProceduralAnimationValues(
				P->GetCrouchAlpha(),
				P->GetDipAlpha(),
				P->GetWalkAlpha(),
				P->GetWalkOffset(),
				P->GetWalkLocationLag(),
				P->GetInAirOffset(),
				P->GetSwayCoef(),
				P->GetLeanCoef()
			);
		}
	}
	
	// else
	return FProceduralAnimationValues();	
}

FWeaponRecoil AFPS_Character::GetWeaponRecoil_Implementation() const
{
	if (GetWeaponComponent() && GetWeaponComponent()->GetWeapon() && GetHealthComponent()->IsAlive())
	{
		return GetWeaponComponent()->GetWeapon()->GetWeaponData().Recoil;
	}
	else return FWeaponRecoil();
}

float AFPS_Character::GetWeaponDispersion_Implementation() const
{
	if (GetWeaponComponent() && GetWeaponComponent()->GetWeapon() && GetHealthComponent()->IsAlive())
	{
		return GetWeaponComponent()->GetWeapon()->GetWeaponState().CurrentDispersion;
	}
	else return 0.f;
}

FAimOffset AFPS_Character::GetAimOffset_Implementation() const
{
	if (GetProceduralAnimComponent() && GetHealthComponent()->IsAlive())
	{
		return GetProceduralAnimComponent()->GetAimOffset();
	}
	else return FAimOffset();
}


//----	Movement	----//	//----	Movement	----//	//----	Movement	----//

void AFPS_Character::UpdateSpeed()
{
	float SlowdownCoef = 1.f;
	if (HealthComponent)
	{
		SlowdownCoef = FMath::GetMappedRangeValueClamped<float>(TRange<float>(0, 100), TRange<float>(SlowdownCoefMin, 1), HealthComponent->GetHealth());
	}
	float NewSpeed = DefaultSpeed * SpeedCoef * SlowdownCoef;
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
	SetSpeed_OnServer(NewSpeed);
}

void AFPS_Character::UpdateSpeed(float NewHealth, float ChangeBy)
{
	UpdateSpeed();
}

void AFPS_Character::SetSpeed_OnServer_Implementation(float New)
{
	SetSpeed_Multicast(New);
}

void  AFPS_Character::SetSpeed_Multicast_Implementation(float New)
{
	GetCharacterMovement()->MaxWalkSpeed = New;
}


//----	Inputs	----//	//----	Inputs	----//	//----	Inputs	----//

void AFPS_Character::ChangeMouseSensivity(float MultiplyBy)
{
	MouseSensivity *= MultiplyBy;
}



void AFPS_Character::InputMove(const FInputActionValue& Value)
{
	FVector2D Input = Value.Get<FVector2D>();
	AddMovementInput(GetActorForwardVector(), Input.Y);
	AddMovementInput(GetActorRightVector(), Input.X);
}

void AFPS_Character::InputLook(const FInputActionValue& Value)
{
	FVector2D Input = Value.Get<FVector2D>() * MouseSensivity;
	AddControllerYawInput(Input.X);
	AddControllerPitchInput(Input.Y);

	// Camera pitch limitations beetwen -85� and 85�
	if (FMath::Abs(GetCamera()->GetComponentRotation().Pitch)>85.f)
	{
		FRotator R = GetCamera()->GetComponentRotation();
		float NewPitch = ((R.Pitch > 0) ? 85.f : -85.f);
		GetCamera()->SetWorldRotation(FRotator(NewPitch, R.Yaw, R.Roll));
	}
}

void AFPS_Character::InputJumpStarted(const FInputActionValue& Value)
{
	if (GetHealthComponent()->IsAlive())
	{
		Jump();
	}
}

void AFPS_Character::InputJumpCompleted(const FInputActionValue& Value)
{
	if (GetHealthComponent()->IsAlive())
	{
		StopJumping();
	}
}

void AFPS_Character::InputCrouch(const FInputActionValue& Value)
{
	bCrouch = Value.Get<bool>();

	if (bCrouch)
	{
		GetWorld()->GetTimerManager().ClearTimer(StandUpCheckTimer);
		GetProceduralAnimComponent()->Crouch();
	}
	else
	{
		GetWorld()->GetTimerManager().SetTimer(StandUpCheckTimer, this, &AFPS_Character::TryStandUp, StandUpCheckTime, true);
	}
}

void AFPS_Character::TryStandUp()
{
	// If character is falling - character can stand up
	bool bCanStandUp = GetCharacterMovement()->IsFalling();

	if (!bCanStandUp)
	{
		FHitResult Hit;
		FVector Start = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + CrouchHeight);
		FVector End = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + StandHeight);
		FCollisionQueryParams TraceParams(FCollisionQueryParams::DefaultQueryParam);
		TraceParams.AddIgnoredActor(this);
	
		// If nothing is above character - character can stand up
		// Checking it by Sphere Trace
		bCanStandUp = !(GetWorld()->SweepSingleByChannel(
			Hit, 
			Start, 
			End, 
			FQuat(), 
			ECC_Visibility, 
			FCollisionShape::MakeSphere(GetCapsuleComponent()->GetScaledCapsuleRadius()),
			TraceParams));
	}

	if (bCanStandUp)
	{
		GetWorld()->GetTimerManager().ClearTimer(StandUpCheckTimer);
		GetProceduralAnimComponent()->StandUp();
	}
}


//---- Procedural Animation Component Events ----//	//---- Procedural Animation Component Events ----//	//---- Procedural Animation Component Events ----//

void AFPS_Character::SetCrouchAlpha_OnServer_Implementation(float NewCrouchAlpha)
{
	SetCrouchAlpha_Multicast(NewCrouchAlpha);
}

void AFPS_Character::SetCrouchAlpha_Multicast_Implementation(float NewCrouchAlpha)
{
	if (!IsLocallyControlled())
	{
		GetProceduralAnimComponent()->SetCrouchAlpha(NewCrouchAlpha);
	}
}

void AFPS_Character::SetLeanCoef_OnServer_Implementation(float NewLeanCoef)
{
	SetLeanCoef_Multicast(NewLeanCoef);
}

void AFPS_Character::SetLeanCoef_Multicast_Implementation(float NewLeanCoef)
{
	if (!IsLocallyControlled())
	{
		GetProceduralAnimComponent()->SetLeanCoef(NewLeanCoef);
	}
}

void AFPS_Character::SetAimOffset_OnServer_Implementation(const FAimOffset& NewAimOffset)
{
	SetAimOffset_Multicast(NewAimOffset);
}

void AFPS_Character::SetAimOffset_Multicast_Implementation(const FAimOffset& NewAimOffset)
{
	if (!IsLocallyControlled())
	{
		GetProceduralAnimComponent()->SetAimOffset(NewAimOffset);
	}
}

void AFPS_Character::SetCapsuleHeight_OnServer_Implementation(float NewCapsuleHeight)
{
	SetCapsuleHeight_Multicast(NewCapsuleHeight);
}

void AFPS_Character::SetCapsuleHeight_Multicast_Implementation(float NewCapsuleHeight)
{
	if (!IsLocallyControlled())
	{
		GetCapsuleComponent()->SetCapsuleHalfHeight(NewCapsuleHeight);
	}
}


//---- Health Component Events ----//	//---- Health Component Events ----//	//---- Health Component Events ----//

void AFPS_Character::NotifyWasHit_Multicast_Implementation(AController* EventIstigator)
{
	UGameplayStatics::PlayWorldCameraShake(GetWorld(), CameraShakeDamage, GetCamera()->GetComponentLocation(), 0.f, 30.f);

	if (EventIstigator && EventIstigator->GetPawn())
	{
		if (EventIstigator->GetPawn()->GetClass()->ImplementsInterface(UFPS_CombatInterface::StaticClass()))
		{
			IFPS_CombatInterface::Execute_NotifyOnHit(EventIstigator->GetPawn());
		}
	}

	//RecieveWasHit(EventIstigator);
	//OnWasHit.Broadcast(EventIstigator);
}



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Interfaces/FPS_ProceduralAnimInterface.h"
#include "../Interfaces/FPS_ProceduralAnimABPInterface.h"
#include "../Interfaces/SetABPArmsVarsInterface.h"
#include "../Interfaces/FPS_CombatInterface.h"

#include "FPS_Character.generated.h"

class UInputAction;
class UCameraComponent;
class UCameraShake;
class USpringArmComponent;
struct FInputActionValue;
class UFPS_ProceduralAnimationComponent;
class UFPS_InventoryComponent;
class UFPS_WeaponComponent;
class UFPS_CharacterHealthComponent;

UCLASS()
class FPS_API AFPS_Character : public ACharacter, 
	public IFPS_ProceduralAnimInterface, 
	public IFPS_ProceduralAnimABPInterface, 
	public IFPS_CombatInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPS_Character();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventIstigator, AActor* DamageCauser) override;

private:

	//----	First-Person Structure	----//

	// Root component for all 1P structure. 
	// Responsible for gameplay-important offsets (Leaning, Jumping)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USceneComponent> Root_1P;

	// Additional component for animations, that impact camera and arms (like Breath system)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USceneComponent> RootShake_1P;

	// Root component for First-Person skeleton
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USpringArmComponent> MeshRoot_1P;

	// Character Arms
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USkeletalMeshComponent> Mesh_1P;

	// Root component for First-Person camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USpringArmComponent> CameraRoot_1P;

	// Empty (1-bone) skeleton as a root component for camera helps with camera animations,
	// updated in Animation Blueprint
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USkeletalMeshComponent> CameraSkel_1P;

	// First-Person Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UCameraComponent> Camera_1P;


	//----	Components	----//

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vars: Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UFPS_ProceduralAnimationComponent> ProceduralAnimComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vars: Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UFPS_InventoryComponent> InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vars: Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UFPS_WeaponComponent> WeaponComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vars: Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UFPS_CharacterHealthComponent> HealthComponent;
	

public:

	//----	Movement	----//

	virtual void OnJumped_Implementation() override;

	virtual void Landed(const FHitResult& Hit) override;


	//---- Damage ----//

	UFUNCTION(BlueprintCallable)
	void Die();

	UFUNCTION(BLueprintImplementableEvent, meta = (DisplayName = "OnDeath"))
	void RecieveOnDeath(AController* DamageInstigator);

	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

	FTimerHandle RagdollTimer;


	//----	Getters and Setters	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Movement")
	bool IsCrouching() const { return bCrouch; }

	// Returns a value indicating the current Character accuracy
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Weapon")
	float GetCurrentAccuracy() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Vitals")
	AController* GetLastDamageInstigator() const { return LastDamageInstigator; }

	UFUNCTION(BlueprintCallable, Category = "FPS_Character: Vitals")
	void SetLastDamageInstigator(AController* DamageInstigator)	 {	if (DamageInstigator) LastDamageInstigator = DamageInstigator;	}

	// Components

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	USceneComponent* GetFirstPersonRoot() const { return Root_1P; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	USceneComponent* GetFirstPersonRootShake() const { return RootShake_1P; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	USpringArmComponent* GetFirstPersonMeshRoot() const { return MeshRoot_1P; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	USkeletalMeshComponent* GetFirstPersonMesh() const { return Mesh_1P; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	USpringArmComponent* GetFirstPersonCameraRoot() const { return CameraRoot_1P; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	UCameraComponent* GetCamera() const { return Camera_1P; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	UFPS_ProceduralAnimationComponent* GetProceduralAnimComponent() const { return ProceduralAnimComponent; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	UFPS_InventoryComponent* GetInventoryComponent() const { return InventoryComponent; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	UFPS_WeaponComponent* GetWeaponComponent() const { return WeaponComponent; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Components")
	UFPS_CharacterHealthComponent* GetHealthComponent() const { return HealthComponent; }


	//----	Interface	----//

	// ICombatInterface

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void NotifyOnHit();
	void NotifyOnHit_Implementation() override;


	// IFPS_ProceduralAnimInterface
	// Interaction between ProceduralAnimationComponent and Character

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ChangeSpeedBy(float MultiplyBy);
	void ChangeSpeedBy_Implementation(float MultiplyBy) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateCapsuleHeight(float CrouchAlpha);
	void UpdateCapsuleHeight_Implementation(float CrouchAlpha) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FVector2D GetLookInput() const;
	FVector2D GetLookInput_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float GetLeanInput() const;
	float GetLeanInput_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetThirdPersonLeftHand() const;
	FTransform GetThirdPersonLeftHand_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	UCameraComponent* GetCamera_1P() const;
	UCameraComponent* GetCamera_1P_Implementation() const override { return Camera_1P; }

	// IFPS_ProceduralAnimABPInterface
	// Interaction between Character and ABP

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetRightHand() const;
	FTransform GetRightHand_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetRightHandCrouchOffset() const;
	FTransform GetRightHandCrouchOffset_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetLeftHand() const;
	FTransform GetLeftHand_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FRotator GetAimRotation() const;
	FRotator GetAimRotation_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FProceduralAnimationValues GetProcAnimValues() const;
	FProceduralAnimationValues GetProcAnimValues_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FWeaponRecoil GetWeaponRecoil() const;
	FWeaponRecoil GetWeaponRecoil_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float GetWeaponDispersion() const;
	float GetWeaponDispersion_Implementation() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FAimOffset GetAimOffset() const;
	FAimOffset GetAimOffset_Implementation() const override;


	//----	Inputs	----//

	UFUNCTION(BlueprintCallable, Category = "FPS_Character: Input")
	void ChangeMouseSensivity(float MultiplyBy);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Input")
	float MouseSensivity = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Input")
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Input")
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Input")
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Input")
	UInputAction* CrouchAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Input")
	UInputAction* LeanAction;


	//----	Camera	----//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Settings")
	TSubclassOf<UCameraShakeBase> CameraShakeFire;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Settings")
	TSubclassOf<UCameraShakeBase> CameraShakeDamage;


	//----	Animations	----//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	TArray<UAnimMontage*> DeathAnimations;


protected:

	//----	Settings	----//

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Settings")
	float StandHeight = 96;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Settings")
	float CrouchHeight = 64;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Settings")
	float DefaultSpeed = 600;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Settings")
	float SlowdownCoefMin = 0.6f;

	
private:

	//----	Movement	----//

	UFUNCTION(BlueprintCallable, Category = "FPS_Character: Movement")
	void UpdateSpeed();

	// Function with parameters matching OnHealthChange delegate
	void UpdateSpeed(float NewHealth, float ChangeBy);

	UFUNCTION(Server, Reliable, Category = "FPS_Character: Movement")
	void SetSpeed_OnServer(float New);

	UFUNCTION(NetMulticast, Reliable, Category = "FPS_Character: Movement")
	void SetSpeed_Multicast(float New);

	// Current multiplyer for movement speed
	float SpeedCoef = 1;


	// Crouch

	UPROPERTY(VisibleAnywhere, Category = "Vars: Movement", BlueprintGetter = IsCrouching)
	bool bCrouch = false;

	FTimerHandle StandUpCheckTimer;

	float StandUpCheckTime = 0.015625f; // 1/64


	//----	Inputs	----//

	void InputMove(const FInputActionValue& Value);
	void InputLook(const FInputActionValue& Value);
	struct FEnhancedInputActionValueBinding* LookActionValue;

	void InputJumpStarted(const FInputActionValue& Value);
	void InputJumpCompleted(const FInputActionValue& Value);


	//----	Crouch	----//

	void InputCrouch(const FInputActionValue& Value);

	// Checks if Character can stand up and initiate StandUp logic
	void TryStandUp();


	//----	Lean	----//

	struct FEnhancedInputActionValueBinding* LeanActionValue;


	//---- Weapon ----//

	UFUNCTION()
	void UpdateWeapon();

	UFUNCTION()
	void TakeRecoilFromFire(int32 CurrentAmmo);


	//----	Vitals	----//

	UPROPERTY(VisibleAnywhere, Category = "Vars: Vitals", BlueprintGetter = GetLastDamageInstigator, BlueprintSetter = SetLastDamageInstigator)
	AController* LastDamageInstigator = nullptr;


	//---- Procedural Animation Component Events ----//

	UFUNCTION(Server, Unreliable)
	void SetCrouchAlpha_OnServer(float NewCrouchAlpha);

	UFUNCTION(NetMulticast, Unreliable)
	void SetCrouchAlpha_Multicast(float NewCrouchAlpha);

	UFUNCTION(Server, Unreliable)
	void SetLeanCoef_OnServer(float NewLeanCoef);

	UFUNCTION(NetMulticast, Unreliable)
	void SetLeanCoef_Multicast(float NewLeanCoef);

	UFUNCTION(Server, Unreliable)
	void SetAimOffset_OnServer(const FAimOffset& NewAimOffset);

	UFUNCTION(NetMulticast, Unreliable)
	void SetAimOffset_Multicast(const FAimOffset& NewAimOffset);

	UFUNCTION(Server, Reliable)
	void SetCapsuleHeight_OnServer(float NewCapsuleHeight);

	UFUNCTION(NetMulticast, Reliable)
	void SetCapsuleHeight_Multicast(float NewCapsuleHeight);


	//---- Healt Component Events ----//

	UFUNCTION(NetMulticast, Reliable)
	void NotifyWasHit_Multicast(AController* EventIstigator);


	// Utility

	// Logic that should be execured at BeginPlay, but must wait for Controller
	void TrueBeginPlay();
	FTimerHandle WaitForController;

};

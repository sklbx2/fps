// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_CharacterHealthComponent.h"

#include "Net/UnrealNetwork.h"


void UFPS_CharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFPS_CharacterHealthComponent, Shield);
}


//----	Health	----//	//----	Health	----//	//----	Health	----//

void UFPS_CharacterHealthComponent::ChangeCurrentHealth_OnServer(float ChangeBy, bool bForce)
{
	if (ChangeBy < 0.f)
	{
		GetWorld()->GetTimerManager().SetTimer(SheildCooldownTimer, this, &UFPS_CharacterHealthComponent::ShieldCooldownEnd, ShieldRecoveryCooldown, false);
		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryTimer);
	}

	if (Shield > 0.f && ChangeBy < 0.f)
	{
		float PrevShield = Shield;

		ChangeCurrentShield_OnServer(ChangeBy);

		if (Shield<=0.f)
		{
			Super::ChangeCurrentHealth_OnServer((ChangeBy+PrevShield)*0.5f);
		}
	}
	else
	{
		Super::ChangeCurrentHealth_OnServer(ChangeBy);
	}
}


//----	Shield	----//	//----	Shield	----//	//----	Shield	----//

void UFPS_CharacterHealthComponent::ChangeCurrentShield_OnServer_Implementation(float ChangeBy)
{
	float ChangeValue = ChangeBy;
	if (ChangeBy < 0)
	{
		ChangeValue *= DamageCoefficient;
	}
	Shield += ChangeValue;

	if (Shield > 100.f)
	{
		Shield = 100.f;
	}
	else if (Shield <= 0.f)
	{
		Shield = 0.f;
		NotifyOnShieldBroke_Multicast();
	}

	NotifyOnShieldChange_Multicast(Shield, ChangeValue);
}


//----	Server	----//		//----	Server	----//		//----	Server	----//

void UFPS_CharacterHealthComponent::NotifyOnShieldChange_Multicast_Implementation(float NewShield, float ChangeBy)
{
	OnShieldChange.Broadcast(NewShield, ChangeBy);
}

void UFPS_CharacterHealthComponent::NotifyOnShieldBroke_Multicast_Implementation()
{
	OnShieldBroke.Broadcast();
}


//----	Shield	----//	//----	Shield	----//	//----	Shield	----//

void UFPS_CharacterHealthComponent::ShieldCooldownEnd()
{
	GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryTimer, this, &UFPS_CharacterHealthComponent::RecoverShield, ShieldRecoveryRate, true);
}

void UFPS_CharacterHealthComponent::RecoverShield()
{
	Shield += ShieldRecoveryValue;
	if (Shield > 100.f)
	{
		Shield = 100.f;
		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryTimer);
	}

	NotifyOnShieldChange_Multicast(Shield, ShieldRecoveryValue);
}

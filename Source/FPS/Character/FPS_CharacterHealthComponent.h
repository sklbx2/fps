// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Interactable/FPS_HealthComponent.h"



#include "FPS_CharacterHealthComponent.generated.h"

/**
 * 
 */


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, NewShield, float, ChangeBy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldBroke);


UCLASS()
class FPS_API UFPS_CharacterHealthComponent : public UFPS_HealthComponent
{
	GENERATED_BODY()

public:

	//----	Health	----//

	void ChangeCurrentHealth_OnServer(float ChangeBy, bool bForce = false) override;


	//----	Shield	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	float GetShield() const { return Shield; }
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetShield(float NewShield) { Shield = NewShield; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	bool HasShield() const { return Shield >= 0.f; }


	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	void ChangeCurrentShield_OnServer(float ChangeBy);

	UFUNCTION(NetMulticast, Unreliable)
	void NotifyOnShieldChange_Multicast(float NewShield, float ChangeBy);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnShieldChange OnShieldChange;

	UFUNCTION(NetMulticast, Reliable)
	void NotifyOnShieldBroke_Multicast();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnShieldBroke OnShieldBroke;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Health")
	float ShieldRecoveryValue = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Health")
	float ShieldRecoveryCooldown = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Health")
	float ShieldRecoveryRate = 0.1f;


private:

	void ShieldCooldownEnd();

	void RecoverShield();


	FTimerHandle SheildCooldownTimer;
	FTimerHandle ShieldRecoveryTimer;

	UPROPERTY(Replicated, BlueprintGetter = GetShield, BlueprintSetter = SetShield)
	float Shield = 100.f;
	
};

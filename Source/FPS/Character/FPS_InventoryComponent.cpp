// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_InventoryComponent.h"
#include "FPS_WeaponComponent.h"
#include "../Weapon/FPS_WeaponBase.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"

#include "Net/UnrealNetwork.h"


// Sets default values for this component's properties
UFPS_InventoryComponent::UFPS_InventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	WeaponSlots = { FWeaponSlot() };
	SetIsReplicatedByDefault(true);
}

void UFPS_InventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UFPS_InventoryComponent, AmmoSlots, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UFPS_InventoryComponent, WeaponSlots, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UFPS_InventoryComponent, OwnerMesh_1P, COND_OwnerOnly);
	DOREPLIFETIME(UFPS_InventoryComponent, OwnerMesh_3P);
	DOREPLIFETIME_CONDITION(UFPS_InventoryComponent, Weapon, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UFPS_InventoryComponent, CurrentWeaponIndex, COND_OwnerOnly);
}


// Called when the game starts
void UFPS_InventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UFPS_InventoryComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (GetOwner() == nullptr)
	{
		return;
	}

	if (APlayerController* PlayerController = Cast<APlayerController>(GetOwner()->GetInstigatorController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->RemoveMappingContext(InventoryMappingContext);
		}
	}
}


// Called every frame
void UFPS_InventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


//---- Main	----//	//---- Main	----//	//---- Main	----//

void UFPS_InventoryComponent::Init_OnServer_Implementation(USkeletalMeshComponent* Mesh_1P, USkeletalMeshComponent* Mesh_3P, UFPS_WeaponComponent* WeaponComponent)
{
	// Setup

	OwnerMesh_1P = Mesh_1P;
	OwnerMesh_3P = Mesh_3P;
	Weapon = WeaponComponent;

	// Create Default Weapon
	Weapon->CreateWeapon(WeaponSlots[CurrentWeaponIndex]);

	Init_OnClient();
}

void UFPS_InventoryComponent::Init_OnClient_Implementation()
{
	// Set up action bindings
	if (APlayerController* PlayerController = Cast<APlayerController>(GetOwner()->GetInstigatorController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			// Set the priority of the mapping to 1, so that it overrides the Jump action with the Fire action when using touch input
			Subsystem->AddMappingContext(InventoryMappingContext, 1);
		}

		if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerController->InputComponent))
		{
			EIC->BindAction(Slot1Action, ETriggerEvent::Triggered, this, &UFPS_InventoryComponent::InputSlot1);
			EIC->BindAction(Slot2Action, ETriggerEvent::Triggered, this, &UFPS_InventoryComponent::InputSlot2);
		}
	}

	OnWeaponSlotChanged.Broadcast();
}



//---- Inventory Logic	----//	//---- Inventory Logic	----//	//---- Inventory Logic	----//

bool UFPS_InventoryComponent::HasAmmo(EWeaponType Type)
{
	for (auto& a : AmmoSlots)
	{
		if (a.Type == Type)
		{
			return a.Amount > 0;
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("UFPS_InventoryComponent::HasAmmo() - AmmoSlots.Find(Type) returned NULL"));
	return  false;

	//if (AmmoSlots.Find(Type))
	//{
	//	return *AmmoSlots.Find(Type) > 0;
	//}
	//else
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("UFPS_InventoryComponent::HasAmmo() - AmmoSlots.Find(Type) returned NULL"));
	//	return  false;
	//}
}

int32 UFPS_InventoryComponent::TakeAmmo(EWeaponType Type, int32 AmoutToTake)
{
	int32 AmmoTaken = 0.f;

	if (AmoutToTake <= 0)
	{
		// In case someone cheating
		UE_LOG(LogTemp, Warning, TEXT("UFPS_InventoryComponent::TakeAmmo() - AmoutToTake <= 0"));
		return AmmoTaken;
	}

	for (auto& a : AmmoSlots)
	{
		if (a.Type == Type)
		{
			if (a.Amount > AmoutToTake)
			{
				AmmoTaken = AmoutToTake;
				a.Amount -= AmoutToTake;
			}
			else
			{
				AmmoTaken = a.Amount;
				a.Amount = 0;
			}
			// Server validation
			ChangeAmmo_OnServer(Type, a.Amount);
			break;
		}
	}

	return AmmoTaken;
	

	/*int32 CurrentAmmo = *AmmoSlots.Find(Type);
	int32 AmmoTaken;

	if (CurrentAmmo > AmoutToTake)
	{
		AmmoTaken = AmoutToTake;
		AmmoSlots.Add(Type, CurrentAmmo - AmoutToTake);
	}
	else
	{
		AmmoTaken = CurrentAmmo;
		AmmoSlots.Add(Type, 0);
	}
	return AmmoTaken;*/
}

bool UFPS_InventoryComponent::CanAddWeapon()
{
	for (auto& w : WeaponSlots)
	{
		if (!w.WeaponClass)
		{
			return true;
		}
	}
	return false;
}

void UFPS_InventoryComponent::OnRep_SetWeaponSlots()
{
	OnWeaponSlotChanged.Broadcast();
}

void UFPS_InventoryComponent::OnRep_SetAmmoSlots()
{
	OnAmmoSlotChanged.Broadcast();
}

void UFPS_InventoryComponent::AddWeapon_OnServer_Implementation(FWeaponSlot WeaponToAdd)
{
	for (auto& w : WeaponSlots)
	{
		if (!w.WeaponClass)
		{
			w = WeaponToAdd;
			OnRep_SetWeaponSlots();
			//NotifyWeaponSlotChanged_Multicast();
			return;
		}
	}
}

void UFPS_InventoryComponent::AddAmmo_OnServer_Implementation(EWeaponType Type, int32 AmountToAdd)
{
	if (AmountToAdd <= 0)
	{
		// In case someone cheating
		UE_LOG(LogTemp, Warning, TEXT("UFPS_InventoryComponent::AddAmmo_OnServer() - AmountToAdd <= 0"));
		return;
	}

	for (auto& a : AmmoSlots)
	{
		if (a.Type == Type)
		{
			a.Amount += AmountToAdd;

			OnRep_SetAmmoSlots();
			//NotifyAmmoSlotChanged_Multicast(Type);
			return;
		}
	}
}


void UFPS_InventoryComponent::ChangeAmmo_OnServer_Implementation(EWeaponType Type, int32 NewAmount)
{
	for (auto& a : AmmoSlots)
	{
		if (a.Type == Type)
		{
			a.Amount = NewAmount;
			return;
		}
	}
}

//void UFPS_InventoryComponent::NotifyAmmoSlotChanged_Multicast_Implementation(EWeaponType Type)
//{
//	OnAmmoSlotChanged.Broadcast(Type);
//}


void UFPS_InventoryComponent::BeginChangeWeapon(int32 NewIndex)
{
	if (WeaponSlots.IsValidIndex(NewIndex) && WeaponSlots[NewIndex].WeaponClass)
	{
		if (NewIndex != CurrentWeaponIndex && !bChangingWeapon)
		{
			StopAllMontages();
			Weapon->GetWeapon()->OnAnimationTriggered.Broadcast(true);
			Weapon->Aim(false);

			bChangingWeapon = true;

			SetNewWeaponIndex_OnServer(NewIndex);

			// Animation
			{
				float PlayRate = 1.f;

				//Character
				{
					// First Person
					if (ChangeWeapon_1P)
					{
						if (auto* AI = OwnerMesh_1P->GetAnimInstance())
						{
							AI->Montage_Play(ChangeWeapon_1P, PlayRate);

							// ChangeWeapon
							if (!(AI->OnPlayMontageNotifyBegin.IsBound()))
							{
								AI->OnPlayMontageNotifyBegin.AddDynamic(this, &UFPS_InventoryComponent::PlayMontageNotifyBegin);
							}
						}
					}

					// Third Person
					if (ChangeWeapon_3P)
					{
						PlayMontage_OnServer(OwnerMesh_3P, ChangeWeapon_3P, PlayRate);
					}
				}
			}

		}
	}
}

//void UFPS_InventoryComponent::NotifyWeaponSlotChanged_Multicast_Implementation()
//{
//	OnWeaponSlotChanged.Broadcast();
//}

void UFPS_InventoryComponent::ChangeWeapon_OnServer_Implementation()
{
	// Saving old weapon in the inventory
	WeaponSlots[CurrentWeaponIndex].WeaponState = Weapon->GetWeapon()->GetWeaponState();

	// Creating new weapon from the inventory
	CurrentWeaponIndex = NewWeaponIndex;
	NewWeaponIndex = -1;

	Weapon->CreateWeapon(WeaponSlots[CurrentWeaponIndex]);
	SetIsChangingWeapon_OnClient(false);
}


//----	Animations	----//

void UFPS_InventoryComponent::PlayMontageNotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload)
{
	if (NotifyName.IsEqual(ChangeWeaponNotifyName))
	{
		ChangeWeapon_OnServer();
	}
}

void UFPS_InventoryComponent::PlayMontage_OnServer_Implementation(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate)
{
	PlayMontage_Multicast(Mesh, AnimMontage, PlayRate);
}

void UFPS_InventoryComponent::PlayMontage_Multicast_Implementation(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate)
{
	if (auto* AI = Mesh->GetAnimInstance())
	{
		AI->Montage_Play(AnimMontage, PlayRate);
	}
}

void UFPS_InventoryComponent::StopAllMontages()
{
	// Update variables

	bChangingWeapon = false;

	// Stop All montages in anim instances
	{
		//Weapon
		{
			if (auto* W = Weapon->GetWeapon())
			{
				// First Person
				if (auto* AI = W->GetFirstPersonMesh()->GetAnimInstance())
				{
					AI->StopAllMontages(1.f);
				}

				// Third Person
				StopAllMontages_OnServer(W->GetThirdPersonMesh());
			}
		}

		//Character
		{
			// First Person
			if (auto* AI = OwnerMesh_1P->GetAnimInstance())
			{
				AI->StopAllMontages(1.f);
			}

			// Third Person
			StopAllMontages_OnServer(OwnerMesh_3P);
		}
	}	
}

void UFPS_InventoryComponent::StopAllMontages_OnServer_Implementation(USkeletalMeshComponent* Mesh)
{
	StopAllMontages_Multicast(Mesh);
}

void UFPS_InventoryComponent::StopAllMontages_Multicast_Implementation(USkeletalMeshComponent* Mesh)
{
	if (auto* AI = Mesh->GetAnimInstance())
	{
		AI->StopAllMontages(1.f);
	}
}






// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Weapon/FPS_WeaponBase.h"


#include "FPS_InventoryComponent.generated.h"

class UInputAction;
struct FInputActionValue;
class UInputMappingContext;
class UFPS_WeaponComponent;

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	EWeaponType Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	int32 Amount;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponSlotChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAmmoSlotChanged);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS_API UFPS_InventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UFPS_InventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	/** Ends gameplay for this component. */
	UFUNCTION()
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	//----	Structure	----//

	UPROPERTY(ReplicatedUsing = OnRep_SetWeaponSlots, EditDefaultsOnly, Category = "Vars: Inventory", BlueprintGetter = GetWeaponSlots)
	TArray<FWeaponSlot> WeaponSlots;

	UPROPERTY(ReplicatedUsing = OnRep_SetAmmoSlots, EditDefaultsOnly, Category = "Vars: Inventory", BlueprintGetter = GetAmmoSlots)
	TArray<FAmmoSlot> AmmoSlots;


public:

	//---- Main	----//

	UFUNCTION(Server, Reliable)
	void Init_OnServer(USkeletalMeshComponent* Mesh_1P, USkeletalMeshComponent* Mesh_3P, UFPS_WeaponComponent* WeaponComponent);

	UFUNCTION(Client, Reliable)
	void Init_OnClient();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponSlotChanged OnWeaponSlotChanged;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoSlotChanged OnAmmoSlotChanged;


	//---- Inventory Logic	----//

	bool HasAmmo(EWeaponType Type);
	int32 TakeAmmo(EWeaponType Type, int32 AmoutToTake);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void AddAmmo_OnServer(EWeaponType Type, int32 AmountToAdd);


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	bool CanAddWeapon();
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void AddWeapon_OnServer(FWeaponSlot WeaponToAdd);


	//----	Getters	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	bool IsChangingWeapon() const { return bChangingWeapon; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	const TArray<FWeaponSlot>& GetWeaponSlots() const { return WeaponSlots; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	const TArray<FAmmoSlot>& GetAmmoSlots() const { return AmmoSlots; }


	//----	Inputs	----//

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Inventory: Input", meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* InventoryMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Inventory: Input")
	UInputAction* Slot1Action;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Inventory: Input")
	UInputAction* Slot2Action;


	//----	Animations	----//

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Inventory: Animations")
	FName ChangeWeaponNotifyName = "ChangeWeapon";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Inventory: Animations")
	UAnimMontage* ChangeWeapon_1P;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Inventory: Animations")
	UAnimMontage* ChangeWeapon_3P;


private:

	//---- Inventory Logic	----//

	void InputSlot1(const FInputActionValue& Value) { BeginChangeWeapon(0); };

	void InputSlot2(const FInputActionValue& Value) { BeginChangeWeapon(1); };

	void BeginChangeWeapon(int32 NewIndex);

	UFUNCTION()
	void OnRep_SetWeaponSlots();

	UFUNCTION()
	void OnRep_SetAmmoSlots();

	UFUNCTION(Server,Reliable)
	void ChangeWeapon_OnServer();

	/*UFUNCTION(NetMulticast, Reliable)
	void NotifyWeaponSlotChanged_Multicast();*/

	UFUNCTION(Server, Reliable)
	void ChangeAmmo_OnServer(EWeaponType Type, int32 NewAmount);

	/*UFUNCTION(NetMulticast, Reliable)
	void NotifyAmmoSlotChanged_Multicast(EWeaponType Type);*/


	//----	Animations	----//

	UFUNCTION()
	void PlayMontageNotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload);

	UFUNCTION(Server, Reliable)
	void PlayMontage_OnServer(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate);

	UFUNCTION(NetMulticast, Reliable)
	void PlayMontage_Multicast(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate);

	void StopAllMontages();

	UFUNCTION(Server, Reliable)
	void StopAllMontages_OnServer(USkeletalMeshComponent* Mesh);

	UFUNCTION(NetMulticast, Reliable)
	void StopAllMontages_Multicast(USkeletalMeshComponent* Mesh);


	//----	References	----//

	UPROPERTY(Replicated)
	UFPS_WeaponComponent* Weapon;

	UPROPERTY(Replicated)
	USkeletalMeshComponent* OwnerMesh_1P;

	UPROPERTY(Replicated)
	USkeletalMeshComponent* OwnerMesh_3P;


	//----	Current	----//

	UPROPERTY(Replicated, VisibleAnywhere, Category = "Vars: Inventory")
	int32 CurrentWeaponIndex = 0;


	UFUNCTION(Server, Reliable)
	void SetNewWeaponIndex_OnServer(int32 Index);
	void SetNewWeaponIndex_OnServer_Implementation(int32 Index) { NewWeaponIndex = Index; }
	int32 NewWeaponIndex;


	UFUNCTION(Client, Reliable)
	void SetIsChangingWeapon_OnClient(bool bChanging);
	void SetIsChangingWeapon_OnClient_Implementation(bool bChanging) { bChangingWeapon = bChanging; }
	UPROPERTY(VisibleAnywhere, Category = "Vars: Inventory", BlueprintGetter = IsChangingWeapon)
	bool bChangingWeapon = false;
};

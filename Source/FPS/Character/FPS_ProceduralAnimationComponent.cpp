// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_ProceduralAnimationComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "Kismet/KismetMathLibrary.h"


// Sets default values for this component's properties
UFPS_ProceduralAnimationComponent::UFPS_ProceduralAnimationComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFPS_ProceduralAnimationComponent::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void UFPS_ProceduralAnimationComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bLocal)
	{
		if (bCrouchAnim)
		{
			CrouchTimeline.TickTimeline(DeltaTime);
		}
		if (bDipAnim)
		{
			DipTimeline.TickTimeline(DeltaTime);
		}
		if (bWalkArmsAnim)
		{
			WalkTimeline.TickTimeline(DeltaTime);
		}
		if (bSwayAnim)
		{
			UpdateSway(DeltaTime);
		}
		UpdateRootShake(DeltaTime);
		if (bLeanAnim)
		{
			UpdateLean(DeltaTime);
		}
		if (bCameraFollowsAnimations)
		{
			UpdateCameraAnimation(DeltaTime);
		}
		UpdateAimOffset(DeltaTime);
	}
}

void UFPS_ProceduralAnimationComponent::Init(
	USceneComponent* MainRoot, 
	USceneComponent* RootShake, 
	USpringArmComponent* CamRoot, 
	USceneComponent* MeshRoot, 
	USceneComponent* Mesh, 
	USkeletalMeshComponent* ThirdPersonMesh,
	UCharacterMovementComponent* CharacterMovement)
{
	MainRootRef = MainRoot;
	RootShakeRef = RootShake;
	CamRootRef = CamRoot;
	MeshRootRef = MeshRoot;
	MeshRef = Mesh;
	ThirdPersonMeshRef = ThirdPersonMesh;

	CharacterMovementRef = CharacterMovement;
	DefaultSpeed = CharacterMovementRef->MaxWalkSpeed;

	if (auto* C = GetOwner()->GetInstigatorController())
	{
		if (C->IsLocalPlayerController())
		{
			bLocal = true;

			// Setting up Crouch
			if (CrouchCurve && bCrouchAnim)
			{
				FOnTimelineFloat CrouchProgress;
				CrouchProgress.BindUFunction(this, FName("UpdateCrouch"));
				CrouchTimeline.AddInterpFloat(CrouchCurve, CrouchProgress);
				CrouchTimeline.SetPlayRate(CrouchSpeed);
			}

			// Setting up Jump
			if (DipCurve && bDipAnim)
			{
				FOnTimelineFloat DipProgress;
				DipProgress.BindUFunction(this, FName("UpdateDip"));
				DipTimeline.AddInterpFloat(DipCurve, DipProgress);
				DipTimeline.SetPlayRate(DipSpeed);
			}

			// Setting up Walk
			if (WalkLeftRightCurve && WalkUpDownCurve && WalkRollCurve && bWalkArmsAnim)
			{
				FOnTimelineFloat WalkProgress;
				WalkProgress.BindUFunction(this, FName("UpdateWalk"));

				// Binding WalkTimeline with WalkUpDownCurve. 
				// LeftRight and Roll curves values calculates in update function
				WalkTimeline.AddInterpFloat(WalkUpDownCurve, WalkProgress);
				WalkTimeline.SetLooping(true);
			}
		}
	}

	WalkTimeline.Play();
}


//----	Crouch	----//	//----	Crouch	----//	//----	Crouch	----//

void UFPS_ProceduralAnimationComponent::Crouch()
{
	CrouchTimeline.Play();
}

void UFPS_ProceduralAnimationComponent::StandUp()
{
	CrouchTimeline.Reverse();
}

void UFPS_ProceduralAnimationComponent::UpdateCrouch(float Value)
{
	//	Calculating old coefficient for speed
	float PrevSpeedCoef = FMath::Lerp(1.f, 0.5f, CrouchAlpha);

	//	Calculating new coefficient for speed
	CrouchAlpha = Value;
	float NewSpeedCoef = FMath::Lerp(1.f, 0.5f, CrouchAlpha);

	//	Calculating multiplyer
	float SpeedMultiplyer = NewSpeedCoef / PrevSpeedCoef;

	// This implementation allows you to change the speed independently from different sources.
	// Like from Crouching and from Aiming
	// 
	// Example:
	// Speed is currently at 0.8 max
	// Now we want to change it to 0.6 max.
	// So we should multiply current speed by (0.6/0.8) and we get 
	//	Speed = 0.8 max * 0.75 =  0.6 max;
	// 
	// Example 2:
	// Character is aiming and crouching. Current speed is:
	//	Speed = max * 0.4 (aiming) * 0.5 (crouching) = 0.2 max;
	// Now we are standing up and crouch coef changes from 0.5 to 0.6;
	// We want to change current speed (0.2 max) to
	//	Speed = max * 0.4 * 0.6 = 0.24 max
	// So we should multiply current speed by (0.6/0.5) and we get
	//	Speed = 0.2 max * 1,2 = 0.24 max ;

	if (GetOwner()->GetClass()->ImplementsInterface(UFPS_ProceduralAnimInterface::StaticClass()))
	{
		// Update Speed
		IFPS_ProceduralAnimInterface::Execute_ChangeSpeedBy(GetOwner(), SpeedMultiplyer);

		// Update Player Capsule Height
		IFPS_ProceduralAnimInterface::Execute_UpdateCapsuleHeight(GetOwner(), CrouchAlpha);
	}

	OnCrouchUpdate.Broadcast(CrouchAlpha);
}


//----	Jump	----//	//----	Jump	----//	//----	Jump	----//

void UFPS_ProceduralAnimationComponent::Dip(bool bLanded)
{
	if (CharacterMovementRef)
	{
		// If playing landing deep - calculating dip strength by Z velocity at landing
		if (bLanded)
		{
			float LastZVelocity = FMath::Abs(CharacterMovementRef->GetLastUpdateVelocity().Z);

			float Strength = UKismetMathLibrary::NormalizeToRange(LastZVelocity, 0, CharacterMovementRef->JumpZVelocity);

			DipStrength = Strength;
		}
		else
		{
			DipStrength = DipDefaultStrength;
		}

		DipTimeline.PlayFromStart();
	}
}

void UFPS_ProceduralAnimationComponent::UpdateDip(float Value)
{
	if (MainRootRef)
	{
		DipAlpha = Value * DipStrength;
		FVector NewLocation = MainRootRef->GetRelativeLocation();
		NewLocation.Z = DipBaseOffset * DipAlpha;;

		MainRootRef->SetRelativeLocation(NewLocation);
	}	
}


//----	Walk	----//	//----	Walk	----//	//----	Walk	----//	

void UFPS_ProceduralAnimationComponent::UpdateWalk(float Value)
{
	// Updating WalkOffset
	{
		// Setting UpDownApha by Value, because WalkTimeline binded with UpDownCurve
		float UpDownAlpha = Value;

		// Calculation LeftRight and Roll manually
		float PlaybackPosition = WalkTimeline.GetPlaybackPosition();
		float LeftRightAlpha = WalkLeftRightCurve->GetFloatValue(PlaybackPosition);
		float RollAlpha = WalkRollCurve->GetFloatValue(PlaybackPosition);

		float Y = FMath::Lerp(WalkLeftRightRange.X, WalkLeftRightRange.Y, LeftRightAlpha);
		float Z = FMath::Lerp(WalkUpDownRange.X, WalkUpDownRange.Y, UpDownAlpha);
		float Roll = FMath::Lerp(WalkRollRange.X, WalkRollRange.Y, RollAlpha);

		WalkOffset = FTransform(FRotator(0, 0, Roll), FVector(0, Y, Z), FVector(1, 1, 1));
	}

	UpdateWalkAlpha();
	UpdateWalkLocationLag();
	UpdateInAirOffset();
	WalkTimeline.SetPlayRate(FMath::Lerp(0, WalkMaxIntensity, WalkAlpha));
}

void UFPS_ProceduralAnimationComponent::UpdateWalkAlpha()
{
	// If character is falling - he's not walking
	if (CharacterMovementRef->IsFalling())
	{
		WalkAlpha = 0.f;
	}

	// Velocity normilized to range from 0 to Max Speed
	WalkAlpha = UKismetMathLibrary::NormalizeToRange(UKismetMathLibrary::VSizeXY(GetOwner()->GetVelocity()), 0, DefaultSpeed);
}

void UFPS_ProceduralAnimationComponent::UpdateWalkLocationLag()
{
	// Syntax sugar
	auto* A = GetOwner();

	// Calculatung XYZ axis components from velocity
	float X = A->GetVelocity().Dot(A->GetActorForwardVector());
	float Y = A->GetVelocity().Dot(A->GetActorRightVector());
	float Z = A->GetVelocity().Dot(A->GetActorUpVector());

	// Turning around (because it is Lag) and normalizing them
	X /= -DefaultSpeed;
	Y /= DefaultSpeed;
	Z /= -(CharacterMovementRef->JumpZVelocity);

	// Applying axis multipliyer
	FVector TargetLocationLag = FVector(X, Y, Z) * WalkLocationLagIntensity;
	// Clamping so that arms do not come out of screen
	TargetLocationLag = TargetLocationLag.GetClampedToSize(0.f, 6.f);

	float DeltaTime = GetWorld()->GetDeltaSeconds();
	float ConstInterpSpeed = 10.f;

	WalkLocationLag = FMath::VInterpTo(WalkLocationLag, TargetLocationLag, DeltaTime, ConstInterpSpeed);
}

void UFPS_ProceduralAnimationComponent::UpdateInAirOffset()
{
	float DeltaTime = GetWorld()->GetDeltaSeconds();
	float ConstInterpSpeed = 5.f;

	FRotator TargetRotator = FRotator(WalkLocationLag.Z * (-5), 0, 0);
	InAirOffset.SetRotation(FQuat(FMath::RInterpTo(InAirOffset.Rotator(), TargetRotator, DeltaTime, ConstInterpSpeed)));

	FVector TargetLocation = FVector(WalkLocationLag.Z*(-1), 0, 0);
	InAirOffset.SetLocation(FMath::VInterpTo(InAirOffset.GetLocation(), TargetLocation, DeltaTime, ConstInterpSpeed));
}


//----	Root Shake	----//	//----	Root Shake	----//	//----	Root Shake	----//

FVector UFPS_ProceduralAnimationComponent::GetBreathOffset(float DeltaTime)
{
	// Calculating breathing speed from current walking state

	// Slow recovery and fast rising up when walking
	float ChangeSpeed = IdleBreathRecoverySpeed + GetWalkAlpha();

	BreathSpeedTarget = BreathSpeedIdle + (GetWalkAlpha() * BreathSpeedWalkMultiplyer);
	BreathSpeedCurrent = FMath::FInterpTo(BreathSpeedCurrent, BreathSpeedTarget, DeltaTime, ChangeSpeed);


	// Changing "breath value" by SIN function

	// To keep BreathSpeed dimension in Hz compensating SIN function delta
	float Correction = PI * 2;
	float BreathDelta = DeltaTime * Correction;
	BreathAlpha += BreathSpeedCurrent * BreathDelta;

	// Sin value clamped to range (0,1);
	float BreathOffsetAlpha = UKismetMathLibrary::MapRangeClamped(FMath::Sin(BreathAlpha), -1.f, 1.f, 0.f, 1.f);

	// Easing
	BreathOffsetAlpha = UKismetMathLibrary::FInterpEaseInOut(-1, 1, BreathOffsetAlpha, 1.5);

	// Offset
	float OffsetStrength = BreathShakeIdleIntensity + (GetWalkAlpha() * BreathShakeWalkingIntensity);
	return FVector(0.f, 0.f, OffsetStrength * BreathOffsetAlpha);
}

void UFPS_ProceduralAnimationComponent::UpdateRootShake(float DeltaTime)
{
	if (RootShakeRef)
	{
		FVector Offset(ForceInitToZero);
		if (bWalkCameraAnim)
		{
			FVector MovementOffset = GetWalkOffset().GetLocation() * WalkShakeIntensity;
			Offset += MovementOffset;
		}
		if (bBreathAnim)
		{
			Offset += GetBreathOffset(DeltaTime);
		}

		RootShakeRef->SetRelativeLocation(Offset);
	}
}


//----	Sway	----//	//----	Sway	----//	//----	Sway	----//

void UFPS_ProceduralAnimationComponent::UpdateSway(float DeltaTime)
{
	if (GetOwner()->GetClass()->ImplementsInterface(UFPS_ProceduralAnimInterface::StaticClass()))
	{
		float ConstInterpSpeed = 1.875f;

		FVector2D Input = IFPS_ProceduralAnimInterface::Execute_GetLookInput(GetOwner());
		
		SwayCoef.X = FMath::FInterpTo(SwayCoef.X, FMath::Clamp(Input.X, -1.f, 1.f), DeltaTime, ConstInterpSpeed);
		SwayCoef.Y = FMath::FInterpTo(SwayCoef.Y, FMath::Clamp(Input.Y, -1.f, 1.f), DeltaTime, ConstInterpSpeed);
	}
}


//----	Lean	----//	//----	Lean	----//	//----	Lean	----//

void UFPS_ProceduralAnimationComponent::UpdateLean(float DeltaTime)
{
	if (GetOwner()->GetClass()->ImplementsInterface(UFPS_ProceduralAnimInterface::StaticClass()))
	{
		if (MainRootRef && CamRootRef && MeshRootRef)
		{
			float ConstInterpSpeed = 3.75f;

			float Input = IFPS_ProceduralAnimInterface::Execute_GetLeanInput(GetOwner());

			LeanCoef = FMath::FInterpTo(LeanCoef, Input, DeltaTime, ConstInterpSpeed);

			// Main root offset
			FVector NewLocation = MainRootRef->GetRelativeLocation();
			NewLocation.Y = LeanCoef * LeanRootOffset;
			MainRootRef->SetRelativeLocation(NewLocation);

			// Camera roll
			FRotator NewRotation = CamRootRef->GetRelativeRotation();
			NewRotation.Roll = LeanCoef * LeanCameraRoll;
			CamRootRef->SetRelativeRotation(NewRotation);

			// If leaning left
			if (LeanCoef < 0.f)
			{
				// Mesh root offset
				NewLocation = MeshRootRef->GetRelativeLocation();
				NewLocation.Y = LeanCoef * LeanRootOffset;
				MeshRootRef->SetRelativeLocation(NewLocation);
			}

			OnLeanUpdate.Broadcast(LeanCoef);
		}
	}
}


//----	Camera Follows Animations	----//	//----	Camera Follows Animations	----//	//----	Camera Follows Animations	----//

FVector UFPS_ProceduralAnimationComponent::GetLeftHandSpeed(float DeltaTime)
{
	if (MeshRef)
	{
		FVector NewLeftHandLocation = MeshRef->GetSocketTransform(FName("hand_l"), ERelativeTransformSpace::RTS_Component).GetLocation();

		FVector Delta = UKismetMathLibrary::ClampVectorSize(NewLeftHandLocation - LeftHandLocation, 0, AnimationsRange);
		FVector Speed = Delta / DeltaTime;

		// Smoothing LeftHandLocation update
		float ConstInterpSpeed = 30.f;
		LeftHandLocation = FMath::VInterpTo(LeftHandLocation, NewLeftHandLocation, DeltaTime, ConstInterpSpeed);

		return Speed;
	}
	else
	{
		return FVector();
	}
}

void UFPS_ProceduralAnimationComponent::UpdateCameraAnimation(float DeltaTime)
{
	if (CamRootRef)
	{
		float ConstInterpSpeed = 0.25f;
		FVector NewOffset = GetLeftHandSpeed(DeltaTime) * CameraFollowStrength;
		CameraOffset = FMath::VInterpTo(CameraOffset, NewOffset, DeltaTime, ConstInterpSpeed);
		CamRootRef->SetRelativeLocation(CameraOffset);
	}
}


//----	AimOffset	----//

void UFPS_ProceduralAnimationComponent::UpdateAimOffset(float DeltaTime)
{
	FRotator DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(
		GetOwner()->GetActorRotation(),
		GetOwner()->GetInstigator()->GetControlRotation());

	AimOffset.Pitch = FMath::FInterpTo(AimOffset.Pitch, DeltaRotation.Pitch, DeltaTime, 20.f);
	AimOffset.Yaw = FMath::FInterpTo(AimOffset.Yaw, -DeltaRotation.Yaw, DeltaTime, 20.f);

	if (GetOwner()->GetClass()->ImplementsInterface(UFPS_ProceduralAnimInterface::StaticClass()))
	{
		FTransform LeftHand = IFPS_ProceduralAnimInterface::Execute_GetThirdPersonLeftHand(GetOwner());
		AimOffset.LeftHand = UKismetMathLibrary::TInterpTo(AimOffset.LeftHand, LeftHand, DeltaTime, 20.f);
	}

	OnAimOffsetUpdate.Broadcast(AimOffset);
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/TimelineComponent.h"
#include "../Interfaces/FPS_ProceduralAnimInterface.h"


#include "FPS_ProceduralAnimationComponent.generated.h"


class UCurveFloat;
class UCharacterMovementComponent;
class USpringArmComponent;


USTRUCT(BlueprintType)
struct FProceduralAnimationValues
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crouch")
	float CrouchAlpha;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jump")
	float DipAlpha;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk")
	float WalkAlpha;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk")
	FTransform WalkOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk")
	FVector WalkLocationLag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jump")
	FTransform InAirOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Look")
	FVector2D SwayCoef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lean")
	float LeanCoef;
};

USTRUCT(BlueprintType)
struct FAimOffset
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimOffset")
	float Pitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimOffset")
	float Yaw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimOffset")
	FTransform LeftHand;
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCrouchUpdate, float, NewCrouchAlpha);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLeanUpdate, float, NewLeanCoef);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAimOffsetUpdate, const FAimOffset&, NewAimOffset);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS_API UFPS_ProceduralAnimationComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFPS_ProceduralAnimationComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Init(USceneComponent* MainRoot,
		USceneComponent* RootShake,
		USpringArmComponent* CamRoot,
		USceneComponent* MeshRoot, 
		USceneComponent* Mesh,
		USkeletalMeshComponent* ThirdPersonMesh,
		UCharacterMovementComponent* CharacterMovement);

public:

	//----	Crouch	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bCrouchAnim = true;

	// Gradually slows down character speed and decrease character capsule height
	UFUNCTION()
	void Crouch();

	// Gradually returns character speed and character capsule height to normal values
	UFUNCTION()
	void StandUp();

	// Returns how much (from 0 to 1) Character is sitting
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	float GetCrouchAlpha() const { return CrouchAlpha; }

	UFUNCTION(BlueprintCallable, Category = "Procedural Animations")
	void SetCrouchAlpha(float New) { CrouchAlpha = New; }

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Procedural Animations")
	FOnCrouchUpdate OnCrouchUpdate;

	FTimeline CrouchTimeline;

	// Curve used in Crouch Timeline
	UPROPERTY(EditAnywhere, Category = "Vars: Procedural Animations: Crouch")
	UCurveFloat* CrouchCurve;

	// How fast Character sit down and stand up
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Jump")
	float CrouchSpeed = 1.5f;


	//----	Jump	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bDipAnim = true;

	// Offsets root component for jumping and landing animations
	UFUNCTION()
	void Dip(bool bLanded = false);

	// Returns how long the Dip animation has played
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	float GetDipAlpha() const { return DipAlpha; }


	FTimeline DipTimeline;

	UPROPERTY(EditAnywhere, Category = "Vars: Procedural Animations: Jump")
	UCurveFloat* DipCurve;

	// How much arms offsets when DipStrength is 1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Jump")
	float DipBaseOffset = -20;

	// How fast Dip animation is playing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Jump")
	float DipSpeed = 2.f;

	// Multiplyer for arms offset when dip
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Jump")
	float DipDefaultStrength = 3.f;


	//----	Walk	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bWalkArmsAnim = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bWalkCameraAnim = true;

	// Getters

	// Current alpha of walking animation
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	float GetWalkAlpha() const { return WalkAlpha; }

	// Returns arms offset by walking animation
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	FTransform GetWalkOffset() const { return WalkOffset; }

	// Returns arms lag by walking animation
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	FVector GetWalkLocationLag() const { return WalkLocationLag; }

	// Returns arms offset by "in air" animation
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	FTransform GetInAirOffset() const { return InAirOffset; }


	// Timeline

	FTimeline WalkTimeline;

	UPROPERTY(EditAnywhere, Category = "Vars: Procedural Animations: Walk")
	UCurveFloat* WalkLeftRightCurve;

	UPROPERTY(EditAnywhere, Category = "Vars: Procedural Animations: Walk")
	UCurveFloat* WalkUpDownCurve;

	UPROPERTY(EditAnywhere, Category = "Vars: Procedural Animations: Walk")
	UCurveFloat* WalkRollCurve;

	// Settings

	// Range of Left/Right offsets for walking animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Walk")
	FVector2D WalkLeftRightRange = FVector2D(-0.4, 0.4);

	// Range of Up/Downoffsets for walking animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Walk")
	FVector2D WalkUpDownRange = FVector2D(-0.35, 0.2);

	// Range of Roll offsets for walking animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Walk")
	FVector2D WalkRollRange = FVector2D(1.f, -1.f);

	// How much arms position lags when moving
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Walk")
	FVector WalkLocationLagIntensity = FVector(2.f, 1.f, 2.f);

	// Strength of offset by walk animations
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Walk")
	float WalkMaxIntensity = 1.65f;

	// Intensity of camera movement by walking
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Walk")
	float WalkShakeIntensity = 10;


	//----	Breathing	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bBreathAnim = true;

	// Breaths in second when idle
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Breath")
	float BreathSpeedIdle = 0.25f;

	// How musch breathing speed increases when walking
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Breath")
	float BreathSpeedWalkMultiplyer = 1.5f;

	// How fast breathing speed recovers
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Breath")
	float IdleBreathRecoverySpeed = 0.3f;

	// Offset intensity from breathing when idle
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Breath")
	float BreathShakeIdleIntensity = 0.75f;

	// Intensity of additional offset from breathing when walking
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Breath")
	float BreathShakeWalkingIntensity = 1.5f;


	//----	Sway	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bSwayAnim = true;

	// Returns interped and clamped LookInput value
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	FVector2D GetSwayCoef() const { return SwayCoef; }


	//----	Lean	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bLeanAnim = true;

	// Returns how much character is leaning (from -1 to 1)
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	float GetLeanCoef() const { return LeanCoef; }

	UFUNCTION(BlueprintCallable, Category = "Procedural Animations")
	void SetLeanCoef(float New) { LeanCoef = New; }

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Procedural Animations")
	FOnLeanUpdate OnLeanUpdate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Lean")
	float LeanRootOffset = 15.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Lean")
	float LeanCameraRoll = 20.f;

	// Additional arms mesh offset when leaning left
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Lean")
	float LeanLeftArmsOffset = 20.f;


	//----	Camera Follows Animations	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations")
	bool bCameraFollowsAnimations = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Camera Follows Animations")
	float AnimationsRange = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Procedural Animations: Camera Follows Animations")
	float CameraFollowStrength = 0.4f;


	//----	AimOffset	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Procedural Animations")
	const FAimOffset& GetAimOffset() const { return AimOffset; }

	UFUNCTION(BlueprintCallable, Category = "Procedural Animations")
	void SetAimOffset(const FAimOffset& New) { AimOffset = New; }

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Procedural Animations")
	FOnAimOffsetUpdate OnAimOffsetUpdate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars: Procedural Animations: AimOffset")
	FName LeftHandSocket = "LHSocket";


private:

	//----	Crouch	----//

	// Updates CrouchAlpha and Speed slowdown coefficiend, calls functions in owner
	UFUNCTION()
	void UpdateCrouch(float Value);

	UPROPERTY(BlueprintGetter = GetCrouchAlpha, BlueprintSetter = SetCrouchAlpha)
	float CrouchAlpha;


	//----	Jump	----//

	// Updates DipAlpha. Changes MainRoot relative location
	UFUNCTION()
	void UpdateDip(float Value);


	UPROPERTY(BlueprintGetter = GetDipAlpha)
	float DipAlpha;

	float DipStrength = 3;


	//----	Walk	----//
	
	// Updates all walk related variables
	UFUNCTION()
	void UpdateWalk(float Value);

	UFUNCTION()
	void UpdateWalkAlpha();

	UFUNCTION()
	void UpdateWalkLocationLag();

	UFUNCTION()
	void UpdateInAirOffset();


	UPROPERTY(BlueprintGetter = GetWalkOffset)
	FTransform WalkOffset;

	UPROPERTY(BlueprintGetter = GetWalkAlpha)
	float WalkAlpha;

	UPROPERTY(BlueprintGetter = GetWalkLocationLag)
	FVector WalkLocationLag;

	UPROPERTY(BlueprintGetter = GetInAirOffset)
	FTransform InAirOffset;

	// Copy of value in character. Set up in Init()
	float DefaultSpeed;


	//----	Root Shake	----//
	//----	Breathing and Walking	----//

	UFUNCTION()
	FVector GetBreathOffset(float DeltaTime);

	UFUNCTION()
	void UpdateRootShake(float DeltaTime);


	float BreathSpeedTarget;
	float BreathSpeedCurrent;
	float BreathAlpha;


	//----	Sway	----//

	UFUNCTION()
	void UpdateSway(float DeltaTime);


	UPROPERTY(BlueprintGetter = GetSwayCoef)
	FVector2D SwayCoef;


	//----	Lean	----//
		
	UFUNCTION()
	void UpdateLean(float DeltaTime);

	UPROPERTY(BlueprintGetter = GetLeanCoef, BlueprintSetter = SetLeanCoef)
	float LeanCoef;


	//----	Camera Follows Animations	----//

	UFUNCTION()
	FVector GetLeftHandSpeed(float DeltaTime);

	UFUNCTION()
	void UpdateCameraAnimation(float DeltaTime);


	FVector LeftHandLocation;
	FVector CameraOffset;


	//----	AimOffset	----//

	UFUNCTION()
	void UpdateAimOffset(float DeltaTime);

	UPROPERTY(BlueprintGetter = GetAimOffset, BlueprintSetter = SetAimOffset)
	FAimOffset AimOffset;



	//----	References	----//

	UPROPERTY()
	USceneComponent* MainRootRef;

	UPROPERTY()
	USceneComponent* RootShakeRef;

	UPROPERTY()
	USpringArmComponent* CamRootRef;

	UPROPERTY()
	USceneComponent* MeshRootRef;

	UPROPERTY()
	USceneComponent* MeshRef;

	UPROPERTY()
	USkeletalMeshComponent* ThirdPersonMeshRef;

	UPROPERTY()
	UCharacterMovementComponent* CharacterMovementRef;


	//----	Network	----//
	bool bLocal = false;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_WeaponComponent.h"
#include "FPS_InventoryComponent.h"
#include "../Weapon/FPS_WeaponBase.h"
#include "../Interfaces/FPS_ProceduralAnimInterface.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"

#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UFPS_WeaponComponent::UFPS_WeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	SetIsReplicatedByDefault(true);
}

void UFPS_WeaponComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFPS_WeaponComponent, OwnerMesh_1P);
	DOREPLIFETIME(UFPS_WeaponComponent, OwnerMesh_3P);
	DOREPLIFETIME_CONDITION(UFPS_WeaponComponent, Inventory, COND_OwnerOnly);
	DOREPLIFETIME(UFPS_WeaponComponent, CurrentWeapon);
}


// Called when the game starts
void UFPS_WeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UFPS_WeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (GetOwner() == nullptr)
	{
		return;
	}

	if (APlayerController* PlayerController = Cast<APlayerController>(GetOwner()->GetInstigatorController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->RemoveMappingContext(WeaponMappingContext);
		}
	}

	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
}


// Called every frame
void UFPS_WeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


//---- Main	----//	//---- Main	----//	//---- Main	----//

void UFPS_WeaponComponent::Init_OnServer_Implementation(USkeletalMeshComponent* Mesh_1P, USkeletalMeshComponent* Mesh_3P, UFPS_InventoryComponent* InventoryComponent)
{
	// Setup

	OwnerMesh_1P = Mesh_1P;
	OwnerMesh_3P = Mesh_3P;
	Inventory = InventoryComponent;

	Init_OnClient();
}

void UFPS_WeaponComponent::Init_OnClient_Implementation()
{
	// Set up action bindings
	if (APlayerController* PlayerController = Cast<APlayerController>(GetOwner()->GetInstigatorController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			// Set the priority of the mapping to 1, so that it overrides the Jump action with the Fire action when using touch input
			Subsystem->AddMappingContext(WeaponMappingContext, 1);
		}

		if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerController->InputComponent))
		{
			// Reload
			EIC->BindAction(ReloadAction, ETriggerEvent::Started, this, &UFPS_WeaponComponent::InputReload);
			EIC->BindAction(FireAction, ETriggerEvent::Started, this, &UFPS_WeaponComponent::InputFire);
			EIC->BindAction(FireAction, ETriggerEvent::Completed, this, &UFPS_WeaponComponent::InputFire);
			EIC->BindAction(AimAction, ETriggerEvent::Started, this, &UFPS_WeaponComponent::InputAim);
			EIC->BindAction(AimAction, ETriggerEvent::Completed, this, &UFPS_WeaponComponent::InputAim);
		}
	}
}


void UFPS_WeaponComponent::CreateWeapon(const FWeaponSlot& WS)
{
	// Destroying old Weapon
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	// Creating new Weapon

	// Spawn parameters
	FVector SpawnLocation = FVector(0);
	FRotator SpawnRotation = FRotator(0);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = GetOwner();
	SpawnParameters.Instigator = GetOwner()->GetInstigator();

	if (WS.WeaponClass)
	{
		if (auto* Weapon = Cast<AFPS_WeaponBase>(GetWorld()->SpawnActor(WS.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParameters)))
		{
			Weapon->SetReplicates(true);
			Weapon->SetWeaponState(WS.WeaponState);
			SetUpWeapon_OnServer(Weapon);
		}
	}
}

// State

void UFPS_WeaponComponent::SetUpWeapon_OnServer_Implementation(AFPS_WeaponBase* Weapon)
{
	CurrentWeapon = Weapon;
	OnRep_SetUpWeapon();
}

void UFPS_WeaponComponent::OnRep_SetUpWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->SetUp(OwnerMesh_1P, OwnerMesh_3P);
		CurrentWeapon->OnFire.AddDynamic(this, &UFPS_WeaponComponent::PlayFireAnim);
		CurrentWeapon->OnReload.AddDynamic(this, &UFPS_WeaponComponent::NotifyOnEndReload);
		OnWeaponCreated.Broadcast();
		CurrentWeapon->OnAnimationTriggered.Broadcast(false);
	}
}

void UFPS_WeaponComponent::NotifyOnEndReload(int32 CurrentAmmo)
{
	OnEndReload.Broadcast(CurrentAmmo);
}


//---- Weapon Logic	----//	//---- Weapon Logic	----//	//---- Weapon Logic	----//

void UFPS_WeaponComponent::InputReload(const FInputActionValue& Value)
{
	// If not reloading already
	if (!ReloadTimer.IsValid())
	{
		if (CanReloadWeapon())
		{
			StopAllMontages();

			auto* W = GetWeapon();
			W->OnAnimationTriggered.Broadcast(true);
			Aim(false);

			// Play rate of the reload animation is related to the default reload speed value for the weapon
			float PlayRate = FWeaponData().ReloadTime / GetWeapon()->GetWeaponData().ReloadTime;

			GetWorld()->GetTimerManager().SetTimer(ReloadTimer, this, &UFPS_WeaponComponent::EndReload, W->GetWeaponData().ReloadTime, false);

			// Animations
			{
				//Weapon
				{
					// First Person
					if (auto* Anim = W->GetAnimations().Reload.Weapon_1P)
					{
						if (auto* AI = W->GetFirstPersonMesh()->GetAnimInstance())
						{
							AI->Montage_Play(Anim, PlayRate);
						}
					}

					// Third Person
					if (auto* Anim = W->GetAnimations().Reload.Weapon_3P)
					{
						PlayMontage_OnServer(W->GetThirdPersonMesh(), Anim, PlayRate);
					}
				}

				//Character
				{
					// First Person
					if (auto* Anim = W->GetAnimations().Reload.Char_1P)
					{
						if (auto* AI = OwnerMesh_1P->GetAnimInstance())
						{
							AI->Montage_Play(Anim, PlayRate);
						}
					}

					// Third Person
					if (auto* Anim = W->GetAnimations().Reload.Char_3P)
					{
						PlayMontage_OnServer(OwnerMesh_3P, Anim, PlayRate);
					}
				}
			}
		}
	}
}

bool UFPS_WeaponComponent::CanReloadWeapon()
{
	if (auto* W = GetWeapon())
	{
		return Inventory->HasAmmo(W->GetWeaponData().Type) && !(W->IsFull());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UFPS_WeaponComponent::CanReloadWeapon() - GetWeapon() returned NULL"));
		return false;
	}
}

void UFPS_WeaponComponent::EndReload()
{
	if (ReloadTimer.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);

		if (auto* W = GetWeapon())
		{
			W->OnAnimationTriggered.Broadcast(false);

			// Want to take missing ammo in the weapon - (Max-Current)
			// If weapon loaded, result must be +1;
			int32 AmountToTake = W->GetWeaponData().MaxAmmo + !(W->IsEmpty()) - W->GetWeaponState().CurrentAmmo;
			W->Reload(Inventory->TakeAmmo(W->GetWeaponData().Type, AmountToTake));
		}
		else { UE_LOG(LogTemp, Warning, TEXT("UFPS_WeaponComponent::EndReload() - GetWeapon() returned NULL")); }
	}
}


void UFPS_WeaponComponent::InputFire(const FInputActionValue& Value)
{
	if (GetWeapon() && !ReloadTimer.IsValid() && Inventory && !(Inventory->IsChangingWeapon()))
	{
		//StopAllMontages();
		GetWeapon()->SetFire(Value.Get<bool>());
	}
	else { UE_LOG(LogTemp, Warning, TEXT("UFPS_InventoryComponent::InputFire() - GetWeapon() returned NULL")); }
}

void UFPS_WeaponComponent::PlayFireAnim(int32 CurrentAmmo)
{
	if (auto* W = GetWeapon())
	{
		OnWeaponFired.Broadcast(CurrentAmmo);

		// Animations
		{
			float PlayRate = 1.f;

			//Character

			// First Person
			if (auto* Anim = W->GetAnimations().Fire.Char_1P)
			{
				if (auto* AI = OwnerMesh_1P->GetAnimInstance())
				{
					AI->Montage_Play(Anim, PlayRate);
				}
			}

			// Third Person
			if (auto* Anim = W->GetAnimations().Fire.Char_3P)
			{
				PlayMontage_OnServer(OwnerMesh_3P, Anim, PlayRate);
			}
		}
	}
}

void UFPS_WeaponComponent::InputAim(const FInputActionValue& Value)
{
	Aim(Value.Get<bool>());
}

void UFPS_WeaponComponent::Aim(bool bAiming)
{
	//	Calculating old coefficient for speed
	float PrevSpeedCoef = FMath::Lerp(1.f, 0.5f, static_cast<float>(GetWeapon()->IsAiming()));

	//	Calculating new coefficient for speed
	float NewSpeedCoef = FMath::Lerp(1.f, 0.5f, static_cast<float>(bAiming));

	//	Calculating multiplyer
	float SpeedMultiplyer = NewSpeedCoef / PrevSpeedCoef;

	if (GetOwner()->GetClass()->ImplementsInterface(UFPS_ProceduralAnimInterface::StaticClass()))
	{
		// Update Speed
		IFPS_ProceduralAnimInterface::Execute_ChangeSpeedBy(GetOwner(), SpeedMultiplyer);
	}

	GetWeapon()->SetAim(bAiming);

	OnAimTriggered.Broadcast(bAiming);
}


//----	Animations	----//	//----	Animations	----//	//----	Animations	----//

void UFPS_WeaponComponent::PlayMontage_OnServer_Implementation(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate)
{
	PlayMontage_Multicast(Mesh, AnimMontage, PlayRate);
}

void UFPS_WeaponComponent::PlayMontage_Multicast_Implementation(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate)
{
	if (auto* AI = Mesh->GetAnimInstance())
	{
		AI->Montage_Play(AnimMontage, PlayRate);
	}
}

void UFPS_WeaponComponent::StopAllMontages()
{
	// Update variables

	if (ReloadTimer.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);
	}

	// Stop All montages in anim instances
	{
		//Weapon
		{
			if (auto* W = GetWeapon())
			{
				// First Person
				if (auto* AI = W->GetFirstPersonMesh()->GetAnimInstance())
				{
					AI->StopAllMontages(1.f);
				}
				
				// Third Personr
				StopAllMontages_OnServer(W->GetThirdPersonMesh());
				
			}
		}

		//Character
		{
			// First Person
			if (auto* AI = OwnerMesh_1P->GetAnimInstance())
			{
				AI->StopAllMontages(1.f);
			}

			// Third Person
			StopAllMontages_OnServer(OwnerMesh_3P);
		}
	}
}

void UFPS_WeaponComponent::StopAllMontages_OnServer_Implementation(USkeletalMeshComponent* Mesh)
{
	StopAllMontages_Multicast(Mesh);
}

void UFPS_WeaponComponent::StopAllMontages_Multicast_Implementation(USkeletalMeshComponent* Mesh)
{
	if (Mesh)
	{
		if (auto* AI = Mesh->GetAnimInstance())
		{
			AI->StopAllMontages(1.f);
		}
	}
}




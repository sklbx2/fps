// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Weapon/FPS_WeaponBase.h"


#include "FPS_WeaponComponent.generated.h"

//////////////////////////////////////////////////////////////////////////
//																		//
//	This component is responsible for all related to CURRENT weapon		//
//																		//
//////////////////////////////////////////////////////////////////////////

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponCreated);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFired, int32, CurrentAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEndReload, int32, CurrentAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAimTriggered, bool, bAim);

class UInputAction;
struct FInputActionValue;
class UInputMappingContext;
class UFPS_InventoryComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS_API UFPS_WeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFPS_WeaponComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	/** Ends gameplay for this component. */
	UFUNCTION()
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


private:

	//----	Current	----//

	UPROPERTY(ReplicatedUsing = OnRep_SetUpWeapon, VisibleAnywhere, Category = "Vars: Inventory", BlueprintGetter = GetWeapon)
	TObjectPtr<AFPS_WeaponBase> CurrentWeapon;

public:

	//---- Main	----//

	UFUNCTION(Server, Reliable)
	void Init_OnServer(USkeletalMeshComponent* Mesh_1P, USkeletalMeshComponent* Mesh_3P, UFPS_InventoryComponent* InventoryComponent);

	// Creates weapon in hands
	void CreateWeapon(const FWeaponSlot& WS);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnWeaponCreated OnWeaponCreated;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnWeaponFired OnWeaponFired;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnEndReload OnEndReload;

	// Changes speed, calls aiming function in weapon, notifies about aiming
	void Aim(bool bAiming);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnAimTriggered OnAimTriggered;


	//----	Getters	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon")
	AFPS_WeaponBase* GetWeapon() const { return CurrentWeapon; }


	//----	Inputs	----//

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Weapon: Input", meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* WeaponMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Weapon: Input")
	UInputAction* ReloadAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Weapon: Input")
	UInputAction* FireAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Weapon: Input")
	UInputAction* AimAction;


private:

	UFUNCTION(Client, Reliable)
	void Init_OnClient();

	//---- Weapon Logic	----//
	
	//	Inputs

	void InputReload(const FInputActionValue& Value);
	bool CanReloadWeapon();
	void EndReload();

	void InputFire(const FInputActionValue& Value);
	UFUNCTION()
	void PlayFireAnim(int32 CurrentAmmo = 0);

	void InputAim(const FInputActionValue& Value);

	// State
	
	UFUNCTION(Server, Reliable)
	void SetUpWeapon_OnServer(AFPS_WeaponBase* Weapon);

	UFUNCTION()
	void OnRep_SetUpWeapon();

	UFUNCTION()
	void NotifyOnEndReload(int32 CurrentAmmo);
	

	//----	Animations	----//

	UFUNCTION(Server, Reliable)
	void PlayMontage_OnServer(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate);

	UFUNCTION(NetMulticast, Reliable)
	void PlayMontage_Multicast(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate);

	void StopAllMontages();

	UFUNCTION(Server, Reliable)
	void StopAllMontages_OnServer(USkeletalMeshComponent* Mesh);

	UFUNCTION(NetMulticast, Reliable)
	void StopAllMontages_Multicast(USkeletalMeshComponent* Mesh);


	//	Timers

	FTimerHandle ReloadTimer;


	//----	References	----//

	UPROPERTY(Replicated)
	UFPS_InventoryComponent* Inventory;

	UPROPERTY(Replicated)
	USkeletalMeshComponent* OwnerMesh_1P;

	UPROPERTY(Replicated)
	USkeletalMeshComponent* OwnerMesh_3P;
};

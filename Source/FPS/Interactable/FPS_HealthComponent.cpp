// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_HealthComponent.h"

#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UFPS_HealthComponent::UFPS_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UFPS_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UFPS_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


void UFPS_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFPS_HealthComponent, Health);
}



void UFPS_HealthComponent::ChangeCurrentHealth_OnServer_Implementation(float ChangeBy, bool bForce)
{
	if (Health > 0.f || bForce)
	{
		float ChangeValue = ChangeBy;
		if (ChangeBy < 0)
		{
			ChangeValue *= DamageCoefficient;
		}
		Health += ChangeValue;

		if (Health > 100.f)
		{
			Health = 100.f;
		}
		else if (Health <= 0.f)
		{
			Health = 0.f;
			NotifyOnDeath_Multicast();
		}

		NotifyOnHealthChange_Multicast(Health, ChangeValue);
	}
}

//----	Server	----//		//----	Server	----//		//----	Server	----//

void UFPS_HealthComponent::NotifyOnHealthChange_Multicast_Implementation(float NewHealth, float ChangeBy)
{
	OnHealthChange.Broadcast(NewHealth, ChangeBy);
}

void UFPS_HealthComponent::NotifyOnDeath_Multicast_Implementation()
{
	OnDeath.Broadcast();
}



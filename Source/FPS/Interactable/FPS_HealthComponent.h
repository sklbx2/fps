// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"



#include "FPS_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, NewHealth, float, ChangeBy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS_API UFPS_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFPS_HealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	float GetHealth() const { return Health; }
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetHealth(float NewHealth) { Health = NewHealth; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	bool IsAlive() const { return Health > 0.f; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Health")
	float DamageCoefficient = 1.0f;


	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	virtual void ChangeCurrentHealth_OnServer(float ChangeBy, bool bForce = false);

	UFUNCTION(NetMulticast, Reliable)
	void NotifyOnHealthChange_Multicast(float NewHealth, float ChangeBy);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnHealthChange OnHealthChange;

	UFUNCTION(NetMulticast, Reliable)
	void NotifyOnDeath_Multicast();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDeath OnDeath;


protected:

	UPROPERTY(Replicated, BlueprintGetter = GetHealth, BlueprintSetter = SetHealth)
	float Health = 100.f;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Character/FPS_ProceduralAnimationComponent.h"
#include "../Weapon/FPS_WeaponBase.h"
#include "Camera/CameraComponent.h"

#include "FPS_ProceduralAnimABPInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFPS_ProceduralAnimABPInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FPS_API IFPS_ProceduralAnimABPInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetRightHand() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetRightHandCrouchOffset() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetLeftHand() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FRotator GetAimRotation() const;	

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FProceduralAnimationValues GetProcAnimValues() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FWeaponRecoil GetWeaponRecoil() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float GetWeaponDispersion() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FAimOffset GetAimOffset() const;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FPS_ProceduralAnimInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFPS_ProceduralAnimInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FPS_API IFPS_ProceduralAnimInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ChangeSpeedBy(float MultiplyBy);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateCapsuleHeight(float CrouchAlpha);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FVector2D GetLookInput() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float GetLeanInput() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetThirdPersonLeftHand() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	UCameraComponent* GetCamera_1P() const;
};

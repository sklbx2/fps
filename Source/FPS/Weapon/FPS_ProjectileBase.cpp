// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_ProjectileBase.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"

#include "Engine/EngineBaseTypes.h"

// Sets default values
AFPS_ProjectileBase::AFPS_ProjectileBase()
{
	bReplicates = true;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(16.f);
	CollisionSphere->SetCollisionProfileName("Projectile");
	CollisionSphere->SetCanEverAffectNavigation(false);
	CollisionSphere->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionSphere->CanCharacterStepUpOn = ECB_No;
	CollisionSphere->SetNotifyRigidBodyCollision(true); // Simulation Generate Hit Events

	RootComponent = CollisionSphere;

	// Hit event return physMaterial
	CollisionSphere->bReturnMaterialOnMove = true;
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->SetCanEverAffectNavigation(false);
	StaticMesh->SetCollisionProfileName("NoCollision");

	FX = CreateDefaultSubobject<UNiagaraComponent>(TEXT("VFX"));
	FX->SetupAttachment(RootComponent);

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	MovementComponent->UpdatedComponent = RootComponent;
	MovementComponent->InitialSpeed = 1.f;
	MovementComponent->MaxSpeed = 0.f;

	MovementComponent->bRotationFollowsVelocity = true;
	MovementComponent->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AFPS_ProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
	CollisionSphere->OnComponentHit.AddDynamic(this, &AFPS_ProjectileBase::OnProjectileHit);
	//CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AFPS_ProjectileBase::OnProjectileBeginOverlap);
	//CollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AFPS_ProjectileBase::OnProjectileEndOverlap);
}

// Called every frame
void AFPS_ProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFPS_ProjectileBase::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (MovementComponent)
	{
		MovementComponent->Velocity = NewVelocity;
	}
}


void AFPS_ProjectileBase::Init(float DamageMultiplyerValue, float SpeedMultiplyerValue)
{
	MovementComponent->InitialSpeed = ProjectileData.StartSpeed;
	MovementComponent->MaxSpeed = ProjectileData.StartSpeed;

	// Restarting MovementComponent, because it's simulation starts with construction
	MovementComponent->SetVelocityInLocalSpace(FVector(MovementComponent->InitialSpeed, 0, 0));
	MovementComponent->UpdateComponentVelocity();

	this->SetLifeSpan(ProjectileData.LifeTime);
}


void AFPS_ProjectileBase::Impact()
{
	this->Destroy();
}


//----	VFX	----//	//----	VFX	----//	//----	VFX	----//

void AFPS_ProjectileBase::SpawnHitFX(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FHitResult& Hit)
{
	// If actor that was hit have Physical Material
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		// Hit Decals
		if (ProjectileData.HitDecals.Contains(SurfaceType))
		{
			UMaterialInterface* HitDecal = ProjectileData.HitDecals[SurfaceType];
			if (HitDecal && OtherComp)
			{
				SpawnHitDecal_Multicast(HitDecal, OtherComp, Hit);
			}
		}
		// Hit FX
		if (ProjectileData.HitFX.Contains(SurfaceType))
		{
			if (UNiagaraSystem* HitFX = ProjectileData.HitFX[SurfaceType])
			{
				SpawnHitVFX_Multicast(HitFX, Hit);
			}
		}

		// Hit Sound

		// If projectile hits object too weakly - sound won't be played
		// Calculating it using dot product of hit normal and velocity
		// that gives velocity projection to hit normal scaled by velocity size. 
		// The value of min boundary 50.f was obtained empirically
		if (ProjectileData.HitSounds.Contains(SurfaceType) && FMath::Abs(Hit.ImpactNormal.Dot(MovementComponent->Velocity)) > 50.f)
		{
			if (USoundBase* HitSound = ProjectileData.HitSounds[SurfaceType])
			{
				SpawnHitSound_Multicast(HitSound, Hit);
			}	
		}
	}
}

void AFPS_ProjectileBase::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* HitDecal, UPrimitiveComponent* OtherComp, const FHitResult& Hit)
{
	FVector DecalSize = FVector(20.0f);
	float LifeSpan = 10.f;

	UGameplayStatics::SpawnDecalAttached(HitDecal, DecalSize, OtherComp, NAME_None,
		Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, LifeSpan);
}

void AFPS_ProjectileBase::SpawnHitVFX_Multicast_Implementation(UNiagaraSystem* HitFX, const FHitResult& Hit)
{
	if (HasAuthority())
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), HitFX, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	}
	else
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), HitFX, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	}
	
}

void AFPS_ProjectileBase::SpawnHitSound_Multicast_Implementation(USoundBase* HitSound, const FHitResult& Hit)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, Hit.ImpactPoint);
}


//----	Collision Events	----//	//----	Collision Events	----//	//----	Collision Events	----//

void AFPS_ProjectileBase::OnProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != GetOwner())
	{
		if (HasAuthority())
		{
			SpawnHitFX(OtherActor, OtherComp, Hit);

			UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileData.Damage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
		}

		if (/*OtherComp->Mobility == EComponentMobility::Movable &&*/ OtherComp->IsSimulatingPhysics())
		{
			if (auto* Skel = Cast<USkeletalMeshComponent>(OtherComp))
			{
				OtherComp->AddImpulseAtLocation(GetActorForwardVector() * GetProjectileData().Damage * 100.f, Hit.TraceStart, Hit.BoneName);
			}
			else
			{
				OtherComp->AddImpulseAtLocation(GetActorForwardVector() * GetProjectileData().Damage * 1000.f, Hit.TraceStart, Hit.BoneName);
			}
		}

		
	}
	else
	{
		// Check OtherActor != GetOwner() at the begining of this function prevents from dealing damage to yourself. But bullet already hit component and
		// acts accordingly to UProjectileMovementComponent logic. How to reinitilize movement component (for 2 time - first time was at Init() ) is unknown.
		// The easiest solution is to spawn new bullet replacing this one.
		// To minimaze this scenario bullet spawn socket in weapons with projectile must be moved further forward.
		if (HasAuthority())
		{
			FVector StartLocation = GetActorLocation() + GetActorForwardVector()*100.f;
			FRotator Rotation = GetActorRotation();
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParameters.Owner = GetOwner();
			SpawnParameters.Instigator = GetInstigator();

			if (auto* P = Cast<AFPS_ProjectileBase>(GetWorld()->SpawnActor(GetClass(), &StartLocation, &Rotation, SpawnParameters)))
			{
				P->Init(DamageMultiplyer, SpeedMultiplyer);
				P->SetReplicates(true);
			}
		}
	}
	Impact();
}

//void AFPS_ProjectileBase::OnProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
//{
//}
//
//void AFPS_ProjectileBase::OnProjectileEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
//{
//}

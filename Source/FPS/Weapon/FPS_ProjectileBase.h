// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "FPS_ProjectileBase.generated.h"


USTRUCT(BlueprintType)
struct FProjectileData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float LifeTime = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float Damage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float StartSpeed = 2000.0f;

	// Material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit VFX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	// Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit VFX")
	TMap < TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;
	// Effect when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit VFX")
	TMap<TEnumAsByte<EPhysicalSurface>, UNiagaraSystem*> HitFX;
};


UCLASS()
class FPS_API AFPS_ProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPS_ProjectileBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

private:

	//----	Components	----//

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* MovementComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UNiagaraComponent> FX = nullptr;


protected:

	//----	Weapon information	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars", meta = (AllowPrivateAccess = "true"), BlueprintGetter = GetProjectileData)
	FProjectileData ProjectileData;

public:

	//----	Main 	----//

	UFUNCTION(BlueprintCallable)
	void Init(float DamageMultiplyerValue = 1.f, float SpeedMultiplyerValue = 1.f);


	//----	Getters 	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_ProjectileBase")
	FProjectileData GetProjectileData() const { return ProjectileData; }

protected:

	float DamageMultiplyer = 1.f;
	float SpeedMultiplyer = 1.f;


private:

	UFUNCTION()
	virtual void Impact();


	//----	VFX	----//

	UFUNCTION()
	void SpawnHitFX(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FHitResult& Hit);

	// Server

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* HitDecal, UPrimitiveComponent* OtherComp, const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitVFX_Multicast(UNiagaraSystem* HitFX, const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* HitSound, const FHitResult& Hit);


	//----	Collision Events	----//

	UFUNCTION()
	virtual void OnProjectileHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	/*UFUNCTION()
	void OnProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnProjectileEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);*/
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_WeaponBase.h"
#include "Components/ArrowComponent.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"

#include "Net/UnrealNetwork.h"

// Sets default values
AFPS_WeaponBase::AFPS_WeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	Mesh_1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh_1P"));
	Mesh_1P->SetupAttachment(SceneComponent);
	Mesh_1P->SetOnlyOwnerSee(true);
	Mesh_1P->bCastDynamicShadow = false;
	Mesh_1P->CastShadow = false;
	Mesh_1P->SetCollisionProfileName("NoCollision");

	Mesh_3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh_3P"));
	Mesh_3P->SetOwnerNoSee(true);
	Mesh_3P->SetCollisionProfileName("NoCollision");

	// Network
	bReplicates = true;
}

void AFPS_WeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AFPS_WeaponBase, WeaponState, COND_OwnerOnly);
}

// Called when the game starts or when spawned
void AFPS_WeaponBase::BeginPlay()
{
	Super::BeginPlay();


	// Binging DropMesh
	if (auto* AI = GetThirdPersonMesh()->GetAnimInstance())
	{
		if (!(AI->OnPlayMontageNotifyBegin.IsBound()))
		{
			AI->OnPlayMontageNotifyBegin.AddDynamic(this, &AFPS_WeaponBase::PlayMontageNotifyBegin);
		}
	}
}

// Called every frame
void AFPS_WeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWeaponState().Heat > 0)
	{
		DispersionTick(DeltaTime);
	}

}


void AFPS_WeaponBase::SetUp(USceneComponent* OwnerMesh_1P, USceneComponent* OwnerMesh_3P)
{
	// Attach meshes to owner
	FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
	this->AttachToComponent(OwnerMesh_1P, Rule, OwnerSocketName);
	Mesh_3P->AttachToComponent(OwnerMesh_3P, Rule, OwnerSocketName);
}


//----	Fire Logic	----//	//----	Fire Logic	----//	//----	Fire Logic	----//

void AFPS_WeaponBase::SetFire(bool bShouldFire)
{
	bFiring = bShouldFire;
	if (bFiring)
	{
		if (!FireTimer.IsValid())
		{
			GetWorld()->GetTimerManager().SetTimer(FireTimer, this, &AFPS_WeaponBase::Fire, GetWeaponData().RateOfFire, true);
			Fire();
		}
	}
	/*else		// Clearing timer in Fire function by bool bFiring (To prevent abusin clicking faster then rate of fire)
	{
		if (FireTimer.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		}
	}*/
}


//----	Reload Logic	----//	//----	Reload Logic	----//	//----	Reload Logic	----//

void AFPS_WeaponBase::Reload(int32 AmmoToPut)
{
	Reload_OnServer(AmmoToPut);
}

void AFPS_WeaponBase::Reload_OnServer_Implementation(int32 AmmoToPut)
{
	WeaponState.CurrentAmmo += AmmoToPut;
	NotifyOnReload_Multicast(WeaponState.CurrentAmmo);
}

void AFPS_WeaponBase::NotifyOnReload_Multicast_Implementation(int32 CurrentAmmo)
{
	OnReload.Broadcast(CurrentAmmo);
}



//----	Drop Mesh	----//	//----	Drop Mesh	----//	//----	Drop Mesh	----//

void AFPS_WeaponBase::DropMagazine()
{
	auto M = GetWeaponData().MagazineDrop;
	FTransform Transform = GetMagazineSocket(true);
	Transform.SetScale3D(FVector(1, 1, 1));

	// First Person
	InitDropMesh(M.Mesh, Transform, M.LifeTime, M.ImpulseDirection, M.ImpulsePower, M.ImpulseDispersion, M.CustomMass);


	//Third Person
	Transform = GetMagazineSocket(false);
	Transform.SetScale3D(FVector(1, 1, 1));
	InitDropMesh(M.Mesh, Transform, M.LifeTime, M.ImpulseDirection, M.ImpulsePower, M.ImpulseDispersion, M.CustomMass, false);
}

void AFPS_WeaponBase::DropShell()
{
	auto M = GetWeaponData().ShellDrop;
	FTransform Transform = GetShellSocket(true);
	Transform.SetScale3D(FVector(1, 1, 1));

	// First Person
	InitDropMesh(M.Mesh, Transform, M.LifeTime, M.ImpulseDirection, M.ImpulsePower, M.ImpulseDispersion, M.CustomMass);

	//Third Person
	Transform = GetShellSocket(false);
	Transform.SetScale3D(FVector(1, 1, 1));
	InitDropMesh(M.Mesh, Transform, M.LifeTime, M.ImpulseDirection, M.ImpulsePower, M.ImpulseDispersion, M.CustomMass, false);
}


//----	Fire Logic	----//	//----	Fire Logic	----//	//----	Fire Logic	----//

void AFPS_WeaponBase::NotifyOnHit()
{
	RecieveOnHit();

	//OnHitBroadcast();
}

//----	Utility	----//	//----	Utility	----//	//----	Utility	----//

FVector AFPS_WeaponBase::GetDispersedVector(FVector Direction, float Power)
{
	/*if (DebugWeaponShow)
	{
		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), Direction, WeaponData.TraceDistance, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, 5.f, (uint8)'\000', 1.0f);
	}*/
	return FMath::VRandCone(Direction, FMath::Pow(GetWeaponState().CurrentDispersion * 0.5f, Power) * PI / 180.f);
}


//----	Fire Logic	----//	//----	Fire Logic	----//	//----	Fire Logic	----//

void AFPS_WeaponBase::Fire()
{
	if (!bFiring)
	{
		if (FireTimer.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(FireTimer);
			return;
		}
	}

	if (GetWeaponState().CurrentAmmo<=0)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), GetWeaponData().EmptySound, GetActorLocation());
		SetFire(false);
	}
	else
	{
		InitShot();

		DropShell();

		// Animations
		{
			float PlayRate = 1.f;

			// First Person
			if (auto* Anim = GetAnimations().Fire.Weapon_1P)
			{
				if (auto* AI = GetFirstPersonMesh()->GetAnimInstance())
				{
					AI->Montage_Play(Anim, PlayRate);
				}
			}

			// Third Person
			if (auto* Anim = GetAnimations().Fire.Weapon_3P)
			{
				PlayMontage_OnServer(GetThirdPersonMesh(), Anim, PlayRate);
			}
		}
	}
}

void AFPS_WeaponBase::InitShot()
{
	InitShot_OnServer();
}

void AFPS_WeaponBase::InitShot_OnServer_Implementation(const FVector& StartLocation, const FRotator& Rotation)
{
	WeaponState.CurrentAmmo--;
	ChangeHeatByShot();
	NotifyOnFire_Multicast(WeaponState.CurrentAmmo);
}

void AFPS_WeaponBase::NotifyOnFire_Multicast_Implementation(int32 CurrentAmmo)
{
	OnFire.Broadcast(CurrentAmmo);
}


//----	Dispersion Logic	----//	//----	Dispersion Logic	----//	//----	Dispersion Logic	----//

void AFPS_WeaponBase::DispersionTick(float DeltaTime)
{
	const FWeaponDispersion& D = GetWeaponData().Dispersion;
	FWeaponState& W = WeaponState;

	W.Heat -= DeltaTime * D.CoolingSpeed;
	if (W.Heat < 0)
	{
		W.Heat = 0;
	}

	W.CurrentDispersion = D.BaseDispersion + (D.HeatMultiplyer * W.Heat);
}

void AFPS_WeaponBase::ChangeHeatByShot()
{
	const FWeaponDispersion& D = GetWeaponData().Dispersion;
	FWeaponState& W = WeaponState;

	W.Heat += D.HeatPerShot;
	if (W.Heat > 1)
	{
		W.Heat = 1;
	}

	/*if (DebugWeaponShow && GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("CurrentDispersion %f"), WeaponState.CurrentDispersion));
	}*/
}


//----	Drop Mesh	----//	//----	Drop Mesh	----//	//----	Drop Mesh	----//

void AFPS_WeaponBase::InitDropMesh(UStaticMesh* Mesh, FTransform Transform, float LifeTime, 
	FVector ImpulseDirection, float ImpulsePower, float ImpulseDispersion, float CustomMass, bool OnlyOwnerSee)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = this;
	if (auto* Drop = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, SpawnParameters))
	{
		auto* S = Drop->GetStaticMeshComponent();

		Drop->SetCanBeDamaged(true);
		Drop->SetActorTickEnabled(false);
		Drop->SetLifeSpan(LifeTime);
		Drop->SetMobility(EComponentMobility::Movable);

		S->SetCollisionProfileName(FName("BlockAllDynamic"));
		S->SetSimulatePhysics(true);
		
		S->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		// Index "ECC_GameTraceChannel1/2" set for custom channel defined in DefaultEngine.ini
		S->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Ignore);
		S->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECR_Ignore);
		
		S->SetCanEverAffectNavigation(false);
		
		S->SetStaticMesh(Mesh);

		if (OnlyOwnerSee)
		{
			S->SetOnlyOwnerSee(true);
		}
		else
		{
			S->SetOwnerNoSee(true);
		}

		if (CustomMass > 0.f)
		{
			Drop->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass);
		}

		if (!ImpulseDirection.IsNearlyZero())
		{
			float EjectionSpeed = FMath::FRandRange(0.8 * ImpulsePower, 1.2 * ImpulsePower);
			FVector ImpulseVector = FMath::VRandCone(Transform.Rotator().RotateVector(ImpulseDirection),
				ImpulseDispersion * PI / 180.f) * EjectionSpeed;

			S->AddImpulse(ImpulseVector);
		}
	}
}

//void AFPS_WeaponBase::InitDropMesh_OnServer_Implementation(UStaticMesh* Mesh, FTransform Transform, float LifeTime,
//	FVector ImpulseDirection, float ImpulsePower, float ImpulseDispersion, float CustomMass, bool OnlyOwnerSee)
//{
//	InitDropMesh_Multicast(Mesh, Transform, LifeTime, ImpulseDirection, ImpulsePower, ImpulseDispersion, CustomMass, OnlyOwnerSee);
//}
//
//void AFPS_WeaponBase::InitDropMesh_Multicast_Implementation(UStaticMesh* Mesh, FTransform Transform, float LifeTime,
//	FVector ImpulseDirection, float ImpulsePower, float ImpulseDispersion, float CustomMass, bool OnlyOwnerSee)
//{
//	InitDropMesh(Mesh, Transform, LifeTime, ImpulseDirection, ImpulsePower, ImpulseDispersion, CustomMass, OnlyOwnerSee);
//}


//----	Animations 	----//	//----	Animations 	----//	//----	Animations 	----//

void AFPS_WeaponBase::PlayMontageNotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload)
{
	if (NotifyName.IsEqual(MagazineDropNotifyName))
	{
		DropMagazine();
	}
	if (NotifyName.IsEqual(ShellDropNotifyName))
	{
		DropShell();
	}
}

void AFPS_WeaponBase::PlayMontage_OnServer_Implementation(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate)
{
	PlayMontage_Multicast(Mesh, AnimMontage, PlayRate);
}

void AFPS_WeaponBase::PlayMontage_Multicast_Implementation(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate)
{
	if (!(GetInstigator()->IsLocallyControlled()))
	{
		if (auto* AI = Mesh->GetAnimInstance())
		{
			AI->Montage_Play(AnimMontage, PlayRate);
		}
	}
}


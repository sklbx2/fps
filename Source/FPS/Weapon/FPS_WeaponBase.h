// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "FPS_WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFire, int32, CurrentAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnReload, int32, CurrentAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAnimationTriggered, bool, bPlaying);

class UArrowComponent;

USTRUCT(BlueprintType)
struct FWeaponActionAnimations
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstPerson")
	UAnimMontage* Weapon_1P;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstPerson")
	UAnimMontage* Char_1P;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ThirdPerson")
	UAnimMontage* Weapon_3P;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ThirdPerson")
	UAnimMontage* Char_3P;
};

USTRUCT(BlueprintType)
struct FWeaponAnimations
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	FWeaponActionAnimations Reload;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	FWeaponActionAnimations Fire;
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RIFLE UMETA(DisplayName = "Rifle"),
	SNIPER UMETA(DisplayName = "Sniper Rifle"),
};

USTRUCT(BlueprintType)
struct FDropMeshData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	TObjectPtr<UStaticMesh> Mesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float LifeTime = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	FVector ImpulseDirection = FVector(ForceInitToZero);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float ImpulsePower = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float ImpulseDispersion = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float CustomMass = 0.f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float BaseDispersion = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float HeatPerShot = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float HeatMultiplyer = 3.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float CoolingSpeed = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AccuracyDeclinePower = 3.f;
};

USTRUCT(BlueprintType)
struct FWeaponRecoil
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Recoil")
	float RotateUp = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Recoil")
	float RotateSide = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Recoil")
	float KickUp = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Recoil")
	float KickSide = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Recoil")
	float KickBack = 1.0f;

};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	EWeaponType Type = EWeaponType::RIFLE;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	int32 MaxAmmo = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	int32 ProjectilesPerShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	FWeaponDispersion Dispersion;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	FWeaponRecoil Recoil;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meshes ")
	FDropMeshData MagazineDrop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meshes ")
	FDropMeshData ShellDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* EmptySound = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponState
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	int32 CurrentAmmo = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Heat = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Dispersion")
	float CurrentDispersion = 1.f;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TSubclassOf<AFPS_WeaponBase> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	FWeaponState WeaponState;
};


UCLASS()
class FPS_API AFPS_WeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPS_WeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	//----	Components	----//

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USkeletalMeshComponent> Mesh_1P;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USkeletalMeshComponent> Mesh_3P;


	//----	Weapon state	----//

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "Vars", meta = (AllowPrivateAccess = "true"), BlueprintGetter = GetWeaponState, BlueprintSetter = SetWeaponState)
	FWeaponState WeaponState;

protected:

	//----	Weapon information	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars", meta = (AllowPrivateAccess = "true"), BlueprintGetter = GetWeaponData)
	FWeaponData WeaponData;


public:

	// Attaches weapon meshes to declared OwnerSocketName in given meshes
	UFUNCTION()
	void SetUp(USceneComponent* OwnerMesh_1P, USceneComponent* OwnerMesh_3P);


	//----	Weapon State	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase: State")
	bool IsFull() { return WeaponState.CurrentAmmo == WeaponData.MaxAmmo + 1; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase: State")
	bool IsEmpty() { return WeaponState.CurrentAmmo == 0; }


	//----	Fire Logic	----//

	// Acts like Weapon's trigger
	UFUNCTION(BlueprintCallable, Category = "FPS_WeaponBase")
	void SetFire(bool bShouldFire);


	//----	Reload Logic	----//

	/// <summary>
	/// Adds ammo to the weapon
	/// <para> Doesn't do additional checks -> expects correct amount of ammo </para>
	/// </summary>
	/// <param name="AmmoToPut"> - The number of rounds that will be added to the weapon </param>
	UFUNCTION(BlueprintCallable, Category = "FPS_WeaponBase")
	void Reload(int32 AmmoToPut);


	//----	Drop Mesh	----//

	UFUNCTION(BlueprintCallable, Category = "FPS_WeaponBase")
	void DropMagazine();

	UFUNCTION(BlueprintCallable, Category = "FPS_WeaponBase")
	void DropShell();


	//----	Getters and Setters	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase")
	const FWeaponData& GetWeaponData() const { return WeaponData; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase")
	const UTexture2D* GetIcon() const { return Icon; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase")
	const FWeaponAnimations& GetAnimations() const { return Animations; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase")
	const FWeaponState& GetWeaponState() const { return WeaponState; }
	UFUNCTION(BlueprintCallable, Category = "FPS_WeaponBase")
	void SetWeaponState(const FWeaponState& NewState) { WeaponState = NewState; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase")
	bool IsAiming() const { return bAim; }
	UFUNCTION(BlueprintCallable, Category = "FPS_WeaponBase")
	virtual void SetAim(bool bAiming) { bAim = bAiming; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase: Components")
	USkeletalMeshComponent* GetFirstPersonMesh() const { return Mesh_1P; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_WeaponBase: Components")
	USkeletalMeshComponent* GetThirdPersonMesh() const { return Mesh_3P; }

	// Gets Right Hand Transform for this weapon
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Character SetUp")
	FTransform GetRightHand() const { return RightHand; }

	// Gets Right Hand Crouch Offset for this weapon
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Character SetUp")
	FTransform GetRightHandCrouchOffset() const { return RightHandCrouchOffset; }

	// Gets current left hand socket transform
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Sockets and Bones")
	FTransform GetLeftHandSocket(bool bFirstPerson = true) const { return (bFirstPerson ? Mesh_1P : Mesh_3P)->GetSocketTransform(LeftHandSocketName); }

	// Gets current muzzle socket transform
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Sockets and Bones")
	FTransform GetMuzzleSocket(bool bFirstPerson = true) const { return (bFirstPerson ? Mesh_1P : Mesh_3P)->GetSocketTransform(MuzzleSocketName); }

	// Gets current magazine socket transform
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Sockets and Bones")
	FTransform GetMagazineSocket(bool bFirstPerson = true) const { return (bFirstPerson ? Mesh_1P : Mesh_3P)->GetSocketTransform(MagazineSocketName); }

	// Gets current shell socket transform
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FPS_Character: Sockets and Bones")
	FTransform GetShellSocket(bool bFirstPerson = true) const { return (bFirstPerson ? Mesh_1P : Mesh_3P)->GetSocketTransform(ShellSocketName); }


	//----	Events	----//

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnFire OnFire;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnAnimationTriggered OnAnimationTriggered;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FOnReload OnReload;

	virtual void NotifyOnHit();
	UFUNCTION(BLueprintImplementableEvent, meta = (DisplayName = "OnHit"))
	void RecieveOnHit();


	//----	Character SetUp	----//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Character SetUp")
	FTransform RightHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vars: Character SetUp")
	FTransform RightHandCrouchOffset = FTransform(FRotator(0, 0, 30), FVector(4, 2, 5), FVector(1, 1, 1));


	//----	Animations	----//

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Animations")
	FName MagazineDropNotifyName = "MagazineDrop";

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vars: Animations")
	FName ShellDropNotifyName = "ShellDrop";
	

protected:

	//----	Fire Logic	----//

	// Describes the logic behind what should come out of a weapon when fired
	virtual void InitShot();

	UFUNCTION(Server, Reliable)
	virtual void InitShot_OnServer(const FVector& StartLocation = FVector(ForceInitToZero), const FRotator& Rotation = FRotator(ForceInitToZero));

	//----	Utility	----//

	FVector GetDispersedVector(FVector Direction, float Power = 3.f);


	//----	Icon	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars", BlueprintGetter = GetIcon)
	UTexture2D* Icon = nullptr;


	//----	Animations	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vars", meta = (AllowPrivateAccess = "true"), BlueprintGetter = GetAnimations)
	FWeaponAnimations Animations;


	//----	Sockets and Bones	----//

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars: Sockets and Bones")
	FName OwnerSocketName = "WeaponSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars: Sockets and Bones")
	FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars: Sockets and Bones")
	FName LeftHandSocketName = "LHSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars: Sockets and Bones")
	FName MagazineSocketName = "MagazineSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars: Sockets and Bones")
	FName ShellSocketName = "ShellSocket";


private:
		
	//----	Fire Logic	----//
	
	// Spawns projectile with all requried settings
	void Fire();

	bool bFiring;

	UFUNCTION(NetMulticast, Reliable)
	void NotifyOnFire_Multicast(int32 CurrentAmmo);

	FTimerHandle FireTimer;

	UFUNCTION(Server, Reliable)
	void Reload_OnServer(int32 AmmoToPut);

	UFUNCTION(NetMulticast, Reliable)
	void NotifyOnReload_Multicast(int32 CurrentAmmo);

	//----	Dispersion Logic	----//

	UFUNCTION()
	void DispersionTick(float DeltaTime);

	void ChangeHeatByShot();


	//----	Drop Mesh	----//

	UFUNCTION()
	void InitDropMesh(UStaticMesh* Mesh, FTransform Transform, float LifeTime, FVector ImpulseDirection,
		float ImpulsePower, float ImpulseDispersion, float CustomMass, bool OnlyOwnerSee = true);

	//UFUNCTION(Server, Reliable)
	//void InitDropMesh_OnServer(UStaticMesh* Mesh, FTransform Transform, float LifeTime, FVector ImpulseDirection,
	//	float ImpulsePower, float ImpulseDispersion, float CustomMass, bool OnlyOwnerSee = false);

	//UFUNCTION(NetMulticast, Reliable)
	//void InitDropMesh_Multicast(UStaticMesh* Mesh, FTransform Transform, float LifeTime, FVector ImpulseDirection,
	//	float ImpulsePower, float ImpulseDispersion, float CustomMass, bool OnlyOwnerSee = false);


	//----	Aiming 	----//

	UPROPERTY(VisibleAnywhere, Category = "Vars", BlueprintGetter = IsAiming, BlueprintSetter = SetAim)
	bool bAim;


	//----	Animations 	----//

	UFUNCTION()
	void PlayMontageNotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload);

	UFUNCTION(Server, Reliable)
	void PlayMontage_OnServer(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate);

	UFUNCTION(NetMulticast, Reliable)
	void PlayMontage_Multicast(USkeletalMeshComponent* Mesh, UAnimMontage* AnimMontage, float PlayRate);
};

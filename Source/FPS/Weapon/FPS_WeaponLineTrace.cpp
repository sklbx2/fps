// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_WeaponLineTrace.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Camera/CameraComponent.h"
#include "../Interfaces/FPS_ProceduralAnimInterface.h"
#include "Kismet/KismetMathLibrary.h"

int32 DebugWeaponShow = 0;
FAutoConsoleVariableRef CVarWeaponShow(
	TEXT("FPS.DebugWeapon"),
	DebugWeaponShow,
	TEXT("Draw Debug for Weapon"),
	ECVF_Cheat
);

void AFPS_WeaponLineTrace::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner()->GetClass()->ImplementsInterface(UFPS_ProceduralAnimInterface::StaticClass()))
	{
		Camera = IFPS_ProceduralAnimInterface::Execute_GetCamera_1P(GetOwner());
		DefaultFov = Camera->FieldOfView;
	}
}


void AFPS_WeaponLineTrace::InitShot()
{
	FVector SpawnLocation;
	FVector Direction;

	// When aiming line trace from camera directly forward
	if (IsAiming() && Camera)
	{
		SpawnLocation = Camera->GetComponentLocation();
		Direction = Camera->GetForwardVector();
	}
	else
	{
		SpawnLocation = GetMuzzleSocket().GetLocation();

		// Calculate rotation adjustment
		Direction = GetDispersedVector(GetMuzzleSocket().GetUnitAxis(EAxis::X));
	}
	FRotator SpawnRotation = Direction.ToOrientationRotator();

	InitShot_OnServer(SpawnLocation, SpawnRotation);
}

void AFPS_WeaponLineTrace::InitShot_OnServer_Implementation(const FVector& StartLocation, const FRotator& Rotation)
{
	if (GetWeaponState().CurrentAmmo > 0) // Server Validation
	{
		Super::InitShot_OnServer_Implementation(StartLocation, Rotation);

		int8 ProjectilePerShot = GetWeaponData().ProjectilesPerShot;
		TArray<AActor*> ActorsToIgnore;
		ActorsToIgnore.Add(GetOwner());
		FHitResult HitResult;

		FVector EndLocation = StartLocation + Rotation.RotateVector((FVector::ForwardVector * Distance));
		FVector Direction = (EndLocation - StartLocation).GetSafeNormal();

		for (int8 i = 0; i < ProjectilePerShot; i++)
		{
			// Index "ECC_GameTraceChannel2" set for custom channel defined in DefaultEngine.ini
			if (UKismetSystemLibrary::LineTraceSingle(this, StartLocation, EndLocation, UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel2),
				false, ActorsToIgnore, (DebugWeaponShow ? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None), HitResult, true,
				FLinearColor::Red, FLinearColor::Red, 0.5f))
			{
				if (DebugWeaponShow && GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("Trace hit %s"), *(HitResult.GetActor()->GetName())));
				}

				SpawnHitFX(HitResult.GetActor(), HitResult.GetComponent(), HitResult);

				UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), Damage, HitResult.TraceStart, HitResult, GetInstigatorController(), this, NULL);

				OnLineTraceHit_Multicast(HitResult, Direction);
			}
		}
	}
}

void AFPS_WeaponLineTrace::OnLineTraceHit_Multicast_Implementation(const FHitResult& HitResult, const FVector& Direction)
{
	if (HitResult.GetComponent() && HitResult.GetComponent()->IsSimulatingPhysics())
	{
		if (auto* Skel = Cast<USkeletalMeshComponent>(HitResult.GetComponent()))
		{
			Skel->AddImpulseAtLocation(Direction * Damage * 100.f, HitResult.TraceStart, HitResult.BoneName);
		}
		else
		{
			HitResult.GetComponent()->AddImpulseAtLocation(Direction * Damage * 1000.f, HitResult.TraceStart, HitResult.BoneName);
		}
	}
}




void AFPS_WeaponLineTrace::SetAim(bool bAiming)
{
	Super::SetAim(bAiming);

	if (!AimTimer.IsValid())
	{
		GetWorld()->GetTimerManager().SetTimer(AimTimer, this, &AFPS_WeaponLineTrace::AimTick, 1.f/60.f, true);
	}
}


void AFPS_WeaponLineTrace::AimTick()
{
	float DeltaTime = GetWorld()->GetDeltaSeconds();
	if (FMath::IsNearlyEqual(Camera->FieldOfView, (IsAiming() ? AimingFov : DefaultFov), 0.001))
	{
		if (AimTimer.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(AimTimer);
		}
	}
	else
	{
		Camera->FieldOfView = FMath::FInterpTo(Camera->FieldOfView, (IsAiming() ? AimingFov : DefaultFov), DeltaTime, AimingSpeed);
	}
}



void AFPS_WeaponLineTrace::SpawnHitFX(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FHitResult& Hit)
{
	// If actor that was hit have Physical Material
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		// Hit Decals
		if (HitDecals.Contains(SurfaceType))
		{
			UMaterialInterface* HitDecal = HitDecals[SurfaceType];
			if (HitDecal && OtherComp)
			{
				SpawnHitDecal_Multicast(HitDecal, OtherComp, Hit);
			}
		}
		// Hit FX
		if (HitFX.Contains(SurfaceType))
		{
			if (UNiagaraSystem* HitVFX = HitFX[SurfaceType])
			{
				SpawnHitVFX_Multicast(HitVFX, Hit);
			}
		}

		// Hit Sound

		if (HitSounds.Contains(SurfaceType))
		{
			if (USoundBase* HitSound = HitSounds[SurfaceType])
			{
				SpawnHitSound_Multicast(HitSound, Hit);
			}
		}
	}
}

void AFPS_WeaponLineTrace::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* HitDecal, UPrimitiveComponent* OtherComp, const FHitResult& Hit)
{
	FVector DecalSize = FVector(20.0f);
	float LifeSpan = 10.f;

	UGameplayStatics::SpawnDecalAttached(HitDecal, DecalSize, OtherComp, NAME_None,
		Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, LifeSpan);
}

void AFPS_WeaponLineTrace::SpawnHitVFX_Multicast_Implementation(UNiagaraSystem* HitVFX, const FHitResult& Hit)
{
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), HitVFX, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
}

void AFPS_WeaponLineTrace::SpawnHitSound_Multicast_Implementation(USoundBase* HitSound, const FHitResult& Hit)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, Hit.ImpactPoint);
}

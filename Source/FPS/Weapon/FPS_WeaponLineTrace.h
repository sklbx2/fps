// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPS_WeaponBase.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"


#include "FPS_WeaponLineTrace.generated.h"


#pragma optimize("", on)


class UCameraComponent;

/**
 * 
 */
UCLASS()
class FPS_API AFPS_WeaponLineTrace : public AFPS_WeaponBase
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

public:

	//----	Getters	----//

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon")
	float GetAimingSpeed() const { return AimingSpeed; }


private:

	//----	Main	----//

	virtual void InitShot() override;

	virtual void InitShot_OnServer_Implementation(const FVector& StartLocation = FVector(ForceInitToZero), const FRotator& Rotation = FRotator(ForceInitToZero)) override;

	UFUNCTION(NetMulticast,Reliable)
	void OnLineTraceHit_Multicast(const FHitResult& HitResult, const FVector& Direction);

	virtual void SetAim(bool bAiming) override;

	UFUNCTION()
	void AimTick();


	//----	VFX	----//

	UFUNCTION()
	void SpawnHitFX(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FHitResult& Hit);

	// Server

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* HitDecal, UPrimitiveComponent* OtherComp, const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitVFX_Multicast(UNiagaraSystem* HitVFX, const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* HitSound, const FHitResult& Hit);



	UPROPERTY(EditDefaultsOnly, Category = "Vars")
	float Damage = 20.f;
	UPROPERTY(EditDefaultsOnly, Category = "Vars")
	float Distance = 20000.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Vars")
	float AimingFov = 30.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Vars", BlueprintGetter = GetAimingSpeed)
	float AimingSpeed = 5.f;

	FTimerHandle AimTimer;

	// Material to decal on hit
	UPROPERTY(EditDefaultsOnly, Category = "Vars: Hit VFX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	// Sound when hit
	UPROPERTY(EditDefaultsOnly, Category = "Vars: Hit VFX")
	TMap < TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;
	// Effect when hit check by surface
	UPROPERTY(EditDefaultsOnly, Category = "Vars: Hit VFX")
	TMap<TEnumAsByte<EPhysicalSurface>, UNiagaraSystem*> HitFX;

	UPROPERTY()
	UCameraComponent* Camera;

	float DefaultFov;

};

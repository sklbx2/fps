// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_WeaponProjectile.h"


void AFPS_WeaponProjectile::InitShot()
{
	FVector SpawnLocation = GetMuzzleSocket().GetLocation();
	//FRotator SpawnRotation = GetMuzzleSocket().Rotator();

	// Calculate rotation adjustment
	FVector Direction = GetDispersedVector(GetMuzzleSocket().GetUnitAxis(EAxis::X), GetWeaponData().Dispersion.AccuracyDeclinePower);
	FRotator SpawnRotation = Direction.ToOrientationRotator();

	// Spawn projectile
	if (Projectile)
	{
		InitShot_OnServer(SpawnLocation, SpawnRotation);
	}	
}

void AFPS_WeaponProjectile::InitShot_OnServer_Implementation(const FVector& StartLocation, const FRotator& Rotation)
{
	// Logic is separated to spawn bullets at right location to shooting player, without calculating all players procedural animations on server side
	if (GetWeaponState().CurrentAmmo > 0) // Server Validation
	{
		Super::InitShot_OnServer_Implementation(StartLocation, Rotation);

		int8 ProjectilePerShot = GetWeaponData().ProjectilesPerShot;

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParameters.Owner = GetOwner();
		SpawnParameters.Instigator = GetInstigator();

		for (int8 i = 0; i < ProjectilePerShot; i++)
		{
			if (auto* P = Cast<AFPS_ProjectileBase>(GetWorld()->SpawnActor(Projectile, &StartLocation, &Rotation, SpawnParameters)))
			{
				P->Init(DamageMultiplyer, SpeedMultiplyer);
				P->SetReplicates(true);
			}
		}
	}
}

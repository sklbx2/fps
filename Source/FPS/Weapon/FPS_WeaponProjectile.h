// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPS_WeaponBase.h"
#include "FPS_ProjectileBase.h"

#include "FPS_WeaponProjectile.generated.h"

struct FActorSpawnParameters;

/**
 * 
 */
UCLASS()
class FPS_API AFPS_WeaponProjectile : public AFPS_WeaponBase
{
	GENERATED_BODY()

private:

	virtual void InitShot() override;

	virtual void InitShot_OnServer_Implementation(const FVector& StartLocation = FVector(ForceInitToZero), const FRotator& Rotation = FRotator(ForceInitToZero)) override;


	UPROPERTY(EditDefaultsOnly, Category = "Vars")
	TSubclassOf<AFPS_ProjectileBase> Projectile = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars")
	float SpeedMultiplyer = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Vars")
	float DamageMultiplyer = 1.f;

};
